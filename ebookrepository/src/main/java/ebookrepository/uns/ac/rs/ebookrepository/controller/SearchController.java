package ebookrepository.uns.ac.rs.ebookrepository.controller;


import ebookrepository.uns.ac.rs.ebookrepository.analysers.SerbianAnalyzer;
import ebookrepository.uns.ac.rs.ebookrepository.model.*;
import ebookrepository.uns.ac.rs.ebookrepository.search.QueryBuilder;
import ebookrepository.uns.ac.rs.ebookrepository.search.ResultRetriever;
import org.apache.log4j.Logger;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/search")
public class SearchController {

    private static final Logger LOGGER = Logger.getLogger(SearchController.class);

    @PostMapping(value = "/regular",consumes = "application/json")
    public ResponseEntity<List<ResultData>> searchTermQuery(@RequestBody SimpleQuery simpleQuery) throws Exception{
        LOGGER.info("POST method, regular search. Field: " + simpleQuery.getField() + ", Value: " + simpleQuery.getValue());
        Query query = QueryBuilder.buildQuery(SearchType.regular,simpleQuery.getField(),simpleQuery.getValue());
        List<RequiredHiglight> rh = new ArrayList<>();
        rh.add(new RequiredHiglight(simpleQuery.getField(),simpleQuery.getValue()));
        List<ResultData> results = ResultRetriever.getResultes(query,rh);

        return new ResponseEntity<>(results,HttpStatus.OK);
    }

    @PostMapping(value = "/queryParser",consumes = "application/json")
    public ResponseEntity<List<ResultData>> search(@RequestBody SimpleQuery simpleQuery) throws Exception{
        QueryParser qp = new QueryParser("title",new SerbianAnalyzer());
        Query query = qp.parse(simpleQuery.getValue());
        List<RequiredHiglight> rh = new ArrayList<>();
        List<ResultData> results = ResultRetriever.getResultes(query,rh);

        return new ResponseEntity<>(results,HttpStatus.OK);
    }

    @PostMapping(value = "/phrase", consumes = "application/json")
    public ResponseEntity<List<ResultData>> searchPhrase(@RequestBody SimpleQuery simpleQuery) throws Exception{
        LOGGER.info("POST method, phrase search. Field: " + simpleQuery.getField() + ", Value: " + simpleQuery.getValue());
        Query query = QueryBuilder.buildQuery(SearchType.phrase,simpleQuery.getField(),simpleQuery.getValue());
        List<RequiredHiglight> rh = new ArrayList<>();
        rh.add(new RequiredHiglight(simpleQuery.getField(),simpleQuery.getValue()));
        List<ResultData> results = ResultRetriever.getResultes(query,rh);
        return  new ResponseEntity<>(results,HttpStatus.OK);
    }

    @PostMapping(value="/fuzzy", consumes="application/json")
    public ResponseEntity<List<ResultData>> searchFuzzy(@RequestBody SimpleQuery simpleQuery) throws Exception {
        LOGGER.info("POST method, fuzzy search. Field: " + simpleQuery.getField() + ", Value: " + simpleQuery.getValue());
        Query query= QueryBuilder.buildQuery(SearchType.fuzzy, simpleQuery.getField(), simpleQuery.getValue());
        List<RequiredHiglight> rh = new ArrayList<RequiredHiglight>();
        rh.add(new RequiredHiglight(simpleQuery.getField(), simpleQuery.getValue()));
        List<ResultData> results = ResultRetriever.getResultes(query, rh);
        return new ResponseEntity<List<ResultData>>(results, HttpStatus.OK);
    }

    @PostMapping(value="/boolean", consumes="application/json")
    public ResponseEntity<List<ResultData>> searchBoolean(@RequestBody AdvancedQuery advancedQuery) throws Exception {
        LOGGER.info("POST method, boolean search. Field1: " + advancedQuery.getField1() + ", Value1: " + advancedQuery.getValue1() +
            ", Field2: " + advancedQuery.getField2() + ", Value2: " + advancedQuery.getValue2() + " - Operation: " + advancedQuery.getOperation());

        Query query1 = QueryBuilder.buildQuery(SearchType.regular, advancedQuery.getField1(), advancedQuery.getValue1());
        Query query2 = QueryBuilder.buildQuery(SearchType.regular, advancedQuery.getField2(), advancedQuery.getValue2());

        BooleanQuery.Builder builder=new BooleanQuery.Builder();
        if(advancedQuery.getOperation().equalsIgnoreCase("AND")){
            builder.add(query1,BooleanClause.Occur.MUST);
            builder.add(query2,BooleanClause.Occur.MUST);
        }else if(advancedQuery.getOperation().equalsIgnoreCase("OR")){
            builder.add(query1,BooleanClause.Occur.SHOULD);
            builder.add(query2,BooleanClause.Occur.SHOULD);
        }

        Query query = builder.build();
        List<RequiredHiglight> rh = new ArrayList<>();
        rh.add(new RequiredHiglight(advancedQuery.getField1(), advancedQuery.getValue1()));
        rh.add(new RequiredHiglight(advancedQuery.getField2(), advancedQuery.getValue2()));
        List<ResultData> results = ResultRetriever.getResultes(query, rh);
        return new ResponseEntity<>(results, HttpStatus.OK);
    }
}
