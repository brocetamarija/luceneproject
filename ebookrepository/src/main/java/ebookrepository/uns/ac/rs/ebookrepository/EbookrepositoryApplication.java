package ebookrepository.uns.ac.rs.ebookrepository;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EbookrepositoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(EbookrepositoryApplication.class, args);
	}

}

