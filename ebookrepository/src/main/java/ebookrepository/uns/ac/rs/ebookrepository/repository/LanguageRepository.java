package ebookrepository.uns.ac.rs.ebookrepository.repository;


import ebookrepository.uns.ac.rs.ebookrepository.entity.Language;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LanguageRepository extends JpaRepository<Language,Integer> {


}
