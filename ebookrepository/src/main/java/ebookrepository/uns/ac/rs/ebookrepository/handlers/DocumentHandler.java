package ebookrepository.uns.ac.rs.ebookrepository.handlers;


import ebookrepository.uns.ac.rs.ebookrepository.model.IndexUnit;
import ebookrepository.uns.ac.rs.ebookrepository.model.UploadModel;

import java.io.File;

public abstract class DocumentHandler {

    public abstract IndexUnit getIndexUnit(File file);
    public abstract String getText(File file);
    public abstract UploadModel getUploadModel(File file);
}
