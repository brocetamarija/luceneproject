package ebookrepository.uns.ac.rs.ebookrepository.model;

public final class ResultData {

    private String title;
    private String author;
    private int category;
    private String keywords;
    private String location;
    private String higlight;

    public ResultData(){
        super();
    }

    public ResultData(String title, String author, int category, String keywords, String location, String higlight) {
        this.title = title;
        this.author = author;
        this.category = category;
        this.keywords = keywords;
        this.location = location;
        this.higlight = higlight;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getHiglight() {
        return higlight;
    }

    public void setHiglight(String higlight) {
        this.higlight = higlight;
    }


}
