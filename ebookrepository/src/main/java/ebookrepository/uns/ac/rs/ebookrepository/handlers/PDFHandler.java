package ebookrepository.uns.ac.rs.ebookrepository.handlers;


import ebookrepository.uns.ac.rs.ebookrepository.model.IndexUnit;
import ebookrepository.uns.ac.rs.ebookrepository.model.UploadModel;

import org.apache.lucene.document.DateTools;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.text.PDFTextStripper;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;



public class PDFHandler extends DocumentHandler {


    @Override
    public IndexUnit getIndexUnit(File file) {
        IndexUnit retVal = new IndexUnit();
        PDDocument pdf = null;
        try {
            PDFParser parser = new PDFParser(new RandomAccessFile(file, "r"));
            parser.parse();
            String text = getText(parser);
            retVal.setText(text);

            pdf = parser.getPDDocument();
            PDDocumentInformation info = pdf.getDocumentInformation();
            String title = "" + info.getTitle();
            retVal.setTitle(title);

            String keywords = "" + info.getKeywords();
            if (keywords != null) {
                String[] splittedKeywords = keywords.split(" ");
                retVal.setKeywords(new ArrayList<>(Arrays.asList(splittedKeywords)));
            }

            retVal.setFilename(file.getCanonicalPath());
            String author = "" + info.getAuthor();
            retVal.setAuthor(author);
            String modificationDate = DateTools.dateToString(new Date(file.lastModified()), DateTools.Resolution.DAY);
            retVal.setFiledate(modificationDate);

            pdf.close();

        } catch (IOException e) {
            System.out.println("Greksa pri konvertovanju dokumenta u pdf");
        }
        return retVal;
    }

    @Override
    public String getText(File file) {
        try{
            PDFParser parser = new PDFParser(new RandomAccessFile(file,"r"));
            parser.parse();
            PDDocument pdf = parser.getPDDocument();
            PDFTextStripper textStripper = new PDFTextStripper();
            String text = textStripper.getText(pdf);

            pdf.close();

            return text;
        }catch (IOException e){
            System.out.println("Greksa pri konvertovanju dokumenta u pdf");
        }
        return null;
    }

    @Override
    public UploadModel getUploadModel(File file) {
        UploadModel retVal = new UploadModel();
        PDDocument pdf = null;
        try{
            PDFParser parser = new PDFParser(new RandomAccessFile(file,"r"));
            parser.parse();

            pdf = parser.getPDDocument();
            PDDocumentInformation info = pdf.getDocumentInformation();
            String title = "" + info.getTitle();
            retVal.setTitle(title);

            String keywords = "" + info.getKeywords();
            retVal.setKeywords(keywords);
            String author = "" + info.getAuthor();
            retVal.setAuthor(author);
            retVal.setFilename(file.getCanonicalPath());


            pdf.close();

        }catch(IOException e){
            System.out.println("Greksa pri konvertovanju dokumenta u pdf");
        }
        return retVal;
    }

    public String getText(PDFParser parser){
        try{
            PDFTextStripper textStripper = new PDFTextStripper();
            PDDocument pdf = parser.getPDDocument();
            String text = textStripper.getText(pdf);

            pdf.close();
            return text;
        }catch (IOException e){
            System.out.println("Greksa pri konvertovanju dokumenta u pdf");
        }
        return null;
    }

}
