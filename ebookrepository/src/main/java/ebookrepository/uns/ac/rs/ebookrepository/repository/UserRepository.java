package ebookrepository.uns.ac.rs.ebookrepository.repository;


import ebookrepository.uns.ac.rs.ebookrepository.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Integer> {
    User findByUsernameAndPassword(String username, String password);

    User findByUsername(String username);
}
