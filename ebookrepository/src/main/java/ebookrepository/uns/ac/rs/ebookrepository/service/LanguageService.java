package ebookrepository.uns.ac.rs.ebookrepository.service;

import ebookrepository.uns.ac.rs.ebookrepository.entity.Language;
import ebookrepository.uns.ac.rs.ebookrepository.repository.LanguageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LanguageService implements LanguageServiceInterface {

    @Autowired
    LanguageRepository languageRepository;


    @Override
    public List<Language> findAll() {
        return languageRepository.findAll();
    }

    @Override
    public Language findOne(Integer id) {
        return languageRepository.getOne(id);
    }
}
