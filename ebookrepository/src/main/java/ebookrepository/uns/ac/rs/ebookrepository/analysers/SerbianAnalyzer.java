package ebookrepository.uns.ac.rs.ebookrepository.analysers;

import ebookrepository.uns.ac.rs.ebookrepository.filters.CyrilicToLatinFilter;
import org.apache.lucene.analysis.*;
import org.apache.lucene.analysis.standard.StandardTokenizer;

import java.io.Reader;

public class SerbianAnalyzer extends Analyzer {


    public static final String[] STOP_WORDS = {
            "i","a","ili","ali","pa","te","da","u","po","na"
    };

    public SerbianAnalyzer(){}

    @Override
    protected TokenStreamComponents createComponents(String s) {
        Tokenizer source = new StandardTokenizer();
        TokenStream result = new CyrilicToLatinFilter(source);
        result = new LowerCaseFilter(result);
        result = new StopFilter(result,StopFilter.makeStopSet(STOP_WORDS));
        return new TokenStreamComponents(source,result){
            @Override
            protected void setReader(final Reader reader){
                super.setReader(reader);
            }
        };
    }
}
