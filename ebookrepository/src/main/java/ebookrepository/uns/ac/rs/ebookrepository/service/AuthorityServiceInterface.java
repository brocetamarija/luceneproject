package ebookrepository.uns.ac.rs.ebookrepository.service;

import ebookrepository.uns.ac.rs.ebookrepository.entity.Authority;

public interface AuthorityServiceInterface {

    Authority findByName(String name);
}
