package ebookrepository.uns.ac.rs.ebookrepository.model;

public enum SearchType {

    regular,
    fuzzy,
    phrase,
    range,
    prefix
}
