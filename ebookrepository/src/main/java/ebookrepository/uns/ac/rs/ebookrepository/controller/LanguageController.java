package ebookrepository.uns.ac.rs.ebookrepository.controller;


import ebookrepository.uns.ac.rs.ebookrepository.dto.LanguageDTO;
import ebookrepository.uns.ac.rs.ebookrepository.entity.Language;
import ebookrepository.uns.ac.rs.ebookrepository.service.LanguageServiceInterface;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping(value = "api/languages")
public class LanguageController {

    private static final Logger LOGGER = Logger.getLogger(LanguageController.class);

    @Autowired
    LanguageServiceInterface languageService;

    @GetMapping
    public ResponseEntity<List<LanguageDTO>> getAll(){
        LOGGER.info("GET method, list of languages");
        List<Language> languages = languageService.findAll();
        List<LanguageDTO> languagesDTO = new ArrayList<>();
        for(Language l:languages) {
            languagesDTO.add(new LanguageDTO(l));
        }

        return new ResponseEntity<>(languagesDTO,HttpStatus.OK);
    }
}
