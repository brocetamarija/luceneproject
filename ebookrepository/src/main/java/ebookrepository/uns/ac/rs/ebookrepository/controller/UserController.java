package ebookrepository.uns.ac.rs.ebookrepository.controller;

import ebookrepository.uns.ac.rs.ebookrepository.dto.UserDTO;
import ebookrepository.uns.ac.rs.ebookrepository.entity.Authority;
import ebookrepository.uns.ac.rs.ebookrepository.entity.User;
import ebookrepository.uns.ac.rs.ebookrepository.service.AuthorityServiceInterface;
import ebookrepository.uns.ac.rs.ebookrepository.service.CategoryServiceInterface;
import ebookrepository.uns.ac.rs.ebookrepository.service.UserServiceInterface;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;

import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value="api/users")
public class UserController {

    private static final Logger LOGGER = Logger.getLogger(UserController.class);

    @Autowired
    UserServiceInterface userService;

    @Autowired
    AuthorityServiceInterface authorityServiceInterface;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    CategoryServiceInterface categoryService;

    //Admin
    @GetMapping
    public ResponseEntity<List<UserDTO>> getUsers(){
        LOGGER.info("GET method, list of all users");
        List<User> users = userService.findAll();
        List<UserDTO> usersDTO = new ArrayList<>();
        for(User u:users) {
            usersDTO.add(new UserDTO(u));
        }
        return new ResponseEntity<>(usersDTO, HttpStatus.OK);
    }

    /*public List<User> getAll() {
        return this.userService.findAll();
    }*/

    @GetMapping(value = "/{id}")
    /*public User getUserById(@PathVariable("id") Integer id){
        return  this.userService.findOne(id);
    }*/
    public ResponseEntity<UserDTO> getUserById(@PathVariable("id") Integer id){
        User user = userService.findOne(id);
        if(user == null) {
            LOGGER.info("GET method, user with id: " + id + "not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        LOGGER.info("GET method, getting user with id: " + id);
        return new ResponseEntity<>(new UserDTO(user),HttpStatus.OK);
    }



    @PostMapping(consumes = "application/json")
    public ResponseEntity<?> saveUser(@Validated @RequestBody UserDTO userDTO,BindingResult result){
        if(result.hasErrors()){
            List<FieldError> errors = result.getFieldErrors();
            LOGGER.info("POST method, save user errors: " + errors.toString());
            return new ResponseEntity<>(errors.toString(), HttpStatus.BAD_REQUEST);
        }

        User user = new User();
        Authority authority = authorityServiceInterface.findByName(userDTO.getAuthority());

        user.setFirst_name(userDTO.getFirst_name());
        user.setLast_name(userDTO.getLast_name());
        user.setUsername(userDTO.getUsername());
        user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        user.getUser_authorities().add(authority);
        user.setUserType(userDTO.getAuthority());
        user.setCategory(categoryService.findOne(userDTO.getCategoryDTO().getId()));

        user = userService.save(user);
        LOGGER.info("POST method, user added successfully.");
        return new ResponseEntity<UserDTO>(new UserDTO(user),HttpStatus.CREATED);
    }

    @PutMapping(value = "/subscribe/{id}",consumes = "application/json")
    public ResponseEntity<UserDTO> subscribe(@RequestBody UserDTO userDTO,@PathVariable("id") Integer id){
        User user = userService.findOne(id);

        if(user == null) {
            LOGGER.info("PUT method, subscribe , user with id: " + id + "not found");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        user.setCategory(categoryService.findOne(userDTO.getCategoryDTO().getId()));

        user = userService.save(user);
        LOGGER.info("PUT method, subscribe , user with id: " + id + "subscribed to category" );
        return new ResponseEntity<>(new UserDTO(user),HttpStatus.OK);
    }

    @PutMapping(value = "/{id}/{auth}",consumes = "application/json")
    public ResponseEntity<?> updateUser(@RequestBody UserDTO userDTO, @PathVariable("id") Integer id,@PathVariable("auth") String auth){
        User user = userService.findOne(id);
        System.err.println(auth);
        Authority authority = authorityServiceInterface.findByName(auth);

        if(user == null) {
            LOGGER.info("PUT method, update, user with id: " + id + "not found." );
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        user.setFirst_name(userDTO.getFirst_name());
        user.setLast_name(userDTO.getLast_name());
        user.setUsername(userDTO.getUsername());
        user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        user.getUser_authorities().clear();
        user.getUser_authorities().add(authority);
        user.setUserType(auth);
        user.setCategory(categoryService.findOne(userDTO.getCategoryDTO().getId()));

        user = userService.save(user);
        LOGGER.info("PUT method, updated user with id: " + id);
        return new ResponseEntity<>(new UserDTO(user),HttpStatus.OK);
    }

    @PutMapping(value = "/{id}",consumes = "application/json")
    public ResponseEntity<?> update(@RequestBody UserDTO userDTO, @PathVariable("id") Integer id){
        User user = userService.findOne(id);
        if(user == null) {
            LOGGER.info("PUT method, update, user with id: " + id + "not found." );
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        user.setFirst_name(userDTO.getFirst_name());
        user.setLast_name(userDTO.getLast_name());
        user.setUsername(userDTO.getUsername());
        user.setCategory(categoryService.findOne(userDTO.getCategoryDTO().getId()));

        user = userService.save(user);
        LOGGER.info("PUT method, updated user with id: " + id);
        return new ResponseEntity<>(new UserDTO(user),HttpStatus.OK);
    }

    //Admin
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deleteEBook(@PathVariable("id") Integer id){
        User user = userService.findOne(id);
        if(user != null) {
            userService.remove(id);
            LOGGER.info("DELETE method, user with id: " + id + "deleted." );
            return new ResponseEntity<>(HttpStatus.OK);
        }else {
            LOGGER.info("DELETE method, user with id: " + id + "not found." );
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/logged")
    public User user(Principal user){
        LOGGER.info("GET method, get logged user");
        return this.userService.findByUsername(user.getName());
    }
}
