package ebookrepository.uns.ac.rs.ebookrepository.controller;


import ebookrepository.uns.ac.rs.ebookrepository.dto.EBookDTO;
import ebookrepository.uns.ac.rs.ebookrepository.entity.EBook;
import ebookrepository.uns.ac.rs.ebookrepository.service.CategoryServiceInterface;
import ebookrepository.uns.ac.rs.ebookrepository.service.EBookServiceInterface;
import ebookrepository.uns.ac.rs.ebookrepository.service.LanguageServiceInterface;
import ebookrepository.uns.ac.rs.ebookrepository.service.UserServiceInterface;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;


@RestController
@RequestMapping(value="api/ebook")
public class EBookController {

    private static final Logger LOGGER = Logger.getLogger(EBookController.class);

    @Autowired
    EBookServiceInterface eBookService;

    @Autowired
    CategoryServiceInterface categoryService;

    @Autowired
    LanguageServiceInterface languageService;

    @Autowired
    UserServiceInterface userService;


    private static String DATA_DIR_PATH;

    static {
        ResourceBundle rb = ResourceBundle.getBundle("application");
        DATA_DIR_PATH = rb.getString("dataDir");
    }

    @GetMapping
    public ResponseEntity<List<EBookDTO>> getEbooks(){
        LOGGER.info("GET method, get list of eBooks");
        List<EBook> ebooks = eBookService.findAll();
        List<EBookDTO> ebooksDTO = new ArrayList<>();
        for(EBook eb:ebooks) {
            ebooksDTO.add(new EBookDTO(eb));
        }

        return new ResponseEntity<>(ebooksDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/bycategory/{name}")
    public ResponseEntity<List<EBookDTO>> getEBooksByCategory(@PathVariable("name") String name){
        LOGGER.info("GET method, get list of eBooks by category: " + name);
        List<EBook> ebooks = eBookService.findByCategory(categoryService.findByName(name));
        List<EBookDTO> ebooksDTO = new ArrayList<>();
        for(EBook eb:ebooks) {
            ebooksDTO.add(new EBookDTO(eb));
        }
        return new ResponseEntity<>(ebooksDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<EBookDTO> getEBookById(@PathVariable("id") Integer id){
        EBook eBook = eBookService.findOne(id);
        if(eBook == null){
            LOGGER.info("GET method, eBook with id: " + id + " not found.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        LOGGER.info("GET method, get eBook with id: " + id);
        return new ResponseEntity<>(new EBookDTO(eBook),HttpStatus.OK);
    }

    @GetMapping(value = "/findByFilename/{filename}")
    public ResponseEntity<EBookDTO> getEBookByFilename(@PathVariable("filename") String filename){
        String path = getResourceFilePath(DATA_DIR_PATH).getAbsolutePath() + "\\" + filename + ".pdf";

        EBook eBook = eBookService.findByFilename(path);
        if(eBook == null){
            LOGGER.info("GET method, eBook with filename: " + filename + " not found.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        LOGGER.info("GET method, get eBook with filename: " + filename);
        return new ResponseEntity<>(new EBookDTO(eBook),HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<?> saveEBook(@Validated @RequestBody EBookDTO eBookDTO, BindingResult result){
        if(result.hasErrors()){
            List<FieldError> errors = result.getFieldErrors();
            LOGGER.info("POST Method, save eBook, errors:  " + errors.toString());
            return new ResponseEntity<>(errors.toString(), HttpStatus.BAD_REQUEST);
        }

        EBook eBook = new EBook();

        eBook.setTitle(eBookDTO.getTitle());
        eBook.setAuthor(eBookDTO.getAuthor());
        eBook.setPublication_year(eBookDTO.getPublication_year());
        eBook.setFilename(eBookDTO.getFilename());
        eBook.setKeywords(eBookDTO.getKeywords());
        eBook.setMIME("application/pdf");
        eBook.setCategory(categoryService.findOne(eBookDTO.getCategoryDTO().getId()));
        eBook.setLanguage(languageService.findOne(eBookDTO.getLanguageDTO().getId()));
        eBook.setUser(userService.findOne(eBookDTO.getUserDTO().getId()));

        eBook = eBookService.save(eBook);
        LOGGER.info("POST Method, eBook successfully added!");
        return new ResponseEntity<>(new EBookDTO(eBook),HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}",consumes = "application/json")
    public ResponseEntity<?> updateEBook(@Validated @RequestBody EBookDTO eBookDTO, @PathVariable("id") Integer id,BindingResult result){
        if(result.hasErrors()){
            List<FieldError> errors = result.getFieldErrors();
            LOGGER.info("PUT Method, update eBook, errors: " + errors.toString());
            return new ResponseEntity<>(errors.toString(), HttpStatus.BAD_REQUEST);
        }

        EBook eBook = eBookService.findOne(id);

        if(eBook == null) {
            LOGGER.info("PUT Method, update eBook, eBook with id: " + id + " not found.");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        eBook.setTitle(eBookDTO.getTitle());
        eBook.setAuthor(eBookDTO.getAuthor());
        eBook.setPublication_year(eBookDTO.getPublication_year());
        eBook.setFilename(eBookDTO.getFilename());
        eBook.setKeywords(eBookDTO.getKeywords());
        eBook.setMIME(eBookDTO.getMIME());
        eBook.setCategory(categoryService.findOne(eBookDTO.getCategoryDTO().getId()));
        eBook.setLanguage(languageService.findOne(eBookDTO.getLanguageDTO().getId()));
        eBook.setUser(userService.findOne(eBookDTO.getUserDTO().getId()));

        eBook = eBookService.save(eBook);

        LOGGER.info("PUT Method, update eBook with id: " + id);
        return new ResponseEntity<>(new EBookDTO(eBook),HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deleteEBook(@PathVariable("id") Integer id){
        EBook eBook = eBookService.findOne(id);
        if(eBook != null) {
            LOGGER.info("DELETE Method, delete eBook with id: " + id);
            eBookService.remove(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }else {
            LOGGER.info("DELETE Method, delete eBook, eBook with id: " + id + " not found.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/deleteByFilename/{filename}")
    public ResponseEntity<Void> deleteEBook(@PathVariable("filename") String filename){
        String path = getResourceFilePath(DATA_DIR_PATH).getAbsolutePath() + "\\" + filename + ".pdf";

        EBook eBook = eBookService.findByFilename(path);
        if(eBook != null) {
            LOGGER.info("DELETE Method, delete eBook with filename: " + filename);
            eBookService.remove(eBook.getId());
            return new ResponseEntity<>(HttpStatus.OK);
        }else {
            LOGGER.info("DELETE Method, delete eBook, eBook with filename: " + filename + " not found.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    private File getResourceFilePath(String path){
        URL url = this.getClass().getClassLoader().getResource(path);
        File file = null;
        try{
            file = new File(url.toURI());
        }catch (URISyntaxException e){
            file = new File(url.getPath());
        }
        return file;
    }
}
