package ebookrepository.uns.ac.rs.ebookrepository.model;

import org.apache.lucene.document.*;

import java.util.ArrayList;
import java.util.List;

public class IndexUnit {

    private String text;
    private String title;
    private List<String> keywords = new ArrayList<>();
    private String filename;
    private String filedate;
    private String author;
    private String language;
    private int category;


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFiledate() {
        return filedate;
    }

    public void setFiledate(String filedate) {
        this.filedate = filedate;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public Document getLuceneDocument(){
        Document doc = new Document();
        doc.add(new TextField("text",text,Field.Store.NO));
        doc.add(new TextField("title",title,Field.Store.YES));
        for(String keyword : keywords){
            doc.add(new TextField("keyword",keyword,Field.Store.YES));
        }
        doc.add(new StringField("filename",filename,Field.Store.YES));
        //doc.add(new TextField("filedate",filedate,Field.Store.YES));
        doc.add(new TextField("author",author,Field.Store.YES));
        doc.add(new TextField("language",language,Field.Store.YES));
        doc.add(new LegacyIntField("category",category,Field.Store.YES));
        return doc;
    }
}
