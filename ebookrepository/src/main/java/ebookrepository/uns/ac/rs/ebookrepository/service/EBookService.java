package ebookrepository.uns.ac.rs.ebookrepository.service;

import ebookrepository.uns.ac.rs.ebookrepository.entity.Category;
import ebookrepository.uns.ac.rs.ebookrepository.entity.EBook;
import ebookrepository.uns.ac.rs.ebookrepository.repository.EBookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EBookService implements EBookServiceInterface {

    @Autowired
    EBookRepository eBookRepository;

    @Override
    public List<EBook> findAll() {
        return eBookRepository.findAll();
    }

    @Override
    public EBook findOne(Integer id) {
        return eBookRepository.getOne(id);
    }

    @Override
    public List<EBook> findByCategory(Category category) {
        return eBookRepository.findByCategory(category);
    }

    @Override
    public EBook save(EBook eBook) {
        return eBookRepository.save(eBook);
    }

    @Override
    public EBook findByFilename(String filename) {
        return eBookRepository.findByFilename(filename);
    }

    @Override
    public void remove(Integer id) {
        eBookRepository.deleteById(id);
    }
}
