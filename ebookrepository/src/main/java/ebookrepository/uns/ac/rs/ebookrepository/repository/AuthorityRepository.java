package ebookrepository.uns.ac.rs.ebookrepository.repository;

import ebookrepository.uns.ac.rs.ebookrepository.entity.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority,Integer> {
    Authority findByName(String name);
}
