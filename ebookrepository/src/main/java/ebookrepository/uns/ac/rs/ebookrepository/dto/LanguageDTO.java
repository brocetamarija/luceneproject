package ebookrepository.uns.ac.rs.ebookrepository.dto;

import ebookrepository.uns.ac.rs.ebookrepository.entity.Language;

import java.io.Serializable;

public class LanguageDTO implements Serializable {

    private Integer id;
    private String name;

    public LanguageDTO(){
        super();
    }

    public LanguageDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public LanguageDTO(Language language){
        this(language.getLanguage_id(),
                language.getName());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
