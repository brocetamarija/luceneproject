package ebookrepository.uns.ac.rs.ebookrepository.search;

import ebookrepository.uns.ac.rs.ebookrepository.analysers.SerbianAnalyzer;
import ebookrepository.uns.ac.rs.ebookrepository.handlers.DocumentHandler;
import ebookrepository.uns.ac.rs.ebookrepository.handlers.PDFHandler;
import ebookrepository.uns.ac.rs.ebookrepository.model.RequiredHiglight;
import ebookrepository.uns.ac.rs.ebookrepository.model.ResultData;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class ResultRetriever {

    private TopScoreDocCollector collector;
    private static int maxHints = 10;

    public ResultRetriever(){
        collector = TopScoreDocCollector.create(10);
    }

    public static void setMaxHints(int maxHints){
        ResultRetriever.maxHints = maxHints;
    }

    public static int getMaxHints(){
        return ResultRetriever.maxHints;
    }

    public static List<ResultData> getResultes(Query query, List<RequiredHiglight> requiredHiglights) throws IOException {
        if (query == null) {
            return null;
        }
        //DirectoryReader reader = null;
        try {
            Directory indexDir = new SimpleFSDirectory(FileSystems.getDefault().getPath(ResourceBundle.getBundle("application").getString("index")));
            DirectoryReader reader = DirectoryReader.open(indexDir);
            IndexSearcher is = new IndexSearcher(reader);
            TopScoreDocCollector collector = TopScoreDocCollector.create(maxHints);
            List<ResultData> results = new ArrayList<ResultData>();
            is.search(query, collector);
            ScoreDoc[] hits = collector.topDocs().scoreDocs;

            ResultData rd;
            Document doc;
            Highlighter hl;
            SerbianAnalyzer sa = new SerbianAnalyzer();

            for (ScoreDoc sd : hits) {
                doc = is.doc(sd.doc);
                String[] allKeywords = doc.getValues("keyword");
                String keywords = "";
                for (String keyword : allKeywords) {
                    keywords += keyword.trim() + " ";
                }
                keywords = keywords.trim();
                String title = doc.get("title");
                String location = doc.get("filename");
                String author = doc.get("author");
                int category = Integer.parseInt(doc.get("category"));
                String higlight = "";
                for (RequiredHiglight rh : requiredHiglights) {
                    hl = new Highlighter(new QueryScorer(query, reader, rh.getFieldName()));
                    try {
                        higlight += hl.getBestFragment(sa, rh.getFieldName(), "" + getDOcumentText(location));
                    } catch (InvalidTokenOffsetsException e) {

                    }
                }
                rd = new ResultData(title, author, category, keywords, location, higlight);
                results.add(rd);
            }
            reader.close();
            return results;
        } catch (IOException e) {
            throw new IllegalArgumentException("U prosledjenom direktorijumu ne postoje indeksi ili je direktorijum zakljucan");
        }
    }

    public String printSearchResults(Query query,File indexDir){
        StringBuilder retVal = new StringBuilder();
        try{
            Directory fsDir = new SimpleFSDirectory(FileSystems.getDefault().getPath(indexDir.getAbsolutePath()));
            DirectoryReader ireader = DirectoryReader.open(fsDir);
            IndexSearcher is = new IndexSearcher(ireader);
            is.search(query,collector);

            ScoreDoc[] hits = collector.topDocs().scoreDocs;
            System.err.println("Found " + hits.length + " document(s) that matched query '" + query + "':");
            for (int i = 0; i<collector.getTotalHits(); i++){
                int docId = hits[i].doc;
                Document doc = is.doc(docId);
                retVal.append("\t" + doc.get("title")+ " (" + doc.get("filedate") + ")\n");
                retVal.append("\t"+doc.get("filename")+"\n\n");
            }

            ireader.close();
        }catch (IOException e){
            retVal.append(e.getMessage() + "\n");
        }

        return retVal.toString();
    }

    private static String getDOcumentText(String location){
        File file = new File(location);
        DocumentHandler handler = getHandler(location);
        return handler.getText(file);
    }

    protected static DocumentHandler getHandler(String filename){
        if(filename.endsWith(".pdf")){
            return new PDFHandler();
        }else{
            return null;
        }
    }
}
