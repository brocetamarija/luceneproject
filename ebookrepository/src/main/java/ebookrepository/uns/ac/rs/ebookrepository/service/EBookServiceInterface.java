package ebookrepository.uns.ac.rs.ebookrepository.service;


import ebookrepository.uns.ac.rs.ebookrepository.entity.Category;
import ebookrepository.uns.ac.rs.ebookrepository.entity.EBook;

import java.util.List;

public interface EBookServiceInterface {

    List<EBook> findAll();
    EBook findOne(Integer id);
    List<EBook> findByCategory(Category category);
    EBook save(EBook eBook);
    EBook findByFilename(String filename);
    void remove(Integer id);
}
