package ebookrepository.uns.ac.rs.ebookrepository.controller;


import ebookrepository.uns.ac.rs.ebookrepository.handlers.PDFHandler;
import ebookrepository.uns.ac.rs.ebookrepository.indexing.Indexer;
import ebookrepository.uns.ac.rs.ebookrepository.model.IndexUnit;

import ebookrepository.uns.ac.rs.ebookrepository.model.UploadModel;
import ebookrepository.uns.ac.rs.ebookrepository.service.EBookServiceInterface;
import org.apache.log4j.Logger;
import org.apache.lucene.index.IndexableField;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@RestController
@RequestMapping("api/indexer")
public class IndexerController {

    private static final Logger LOGGER = Logger.getLogger(IndexerController.class);

    @Autowired
    EBookServiceInterface eBookService;

    private static String DATA_DIR_PATH;

    static {
        ResourceBundle rb = ResourceBundle.getBundle("application");
        DATA_DIR_PATH = rb.getString("dataDir");
    }

    @GetMapping("/reindex")
    public ResponseEntity<String> index() throws IOException{
        File dataDir = getResourceFilePath(DATA_DIR_PATH);

        long start = new Date().getTime();
        int numIndexed = Indexer.getInstance().index(dataDir);
        long end = new Date().getTime();
        String text = "Indexing " + numIndexed + "files took" +(end - start) + " milliseconds";
        return new ResponseEntity<>(text,HttpStatus.OK);
    }

    private File getResourceFilePath(String path){
        URL url = this.getClass().getClassLoader().getResource(path);
        File file = null;
        try{
            file = new File(url.toURI());
        }catch (URISyntaxException e){
            file = new File(url.getPath());
        }
        return file;
    }

    @PostMapping("/index/add")
    public ResponseEntity<String> multiUploadFileModel(@ModelAttribute UploadModel model) {
        try {
            indexUploadedFile(model);
        } catch (IOException e) {
            LOGGER.info("POST method, add index error: " + e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        LOGGER.info("POST method, add index successfully!");
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }

    @PostMapping("/getmodel")
    public ResponseEntity<UploadModel> getModel(@ModelAttribute UploadModel model) throws IOException {
        LOGGER.info("POST method, upload model");
        PDFHandler handler = new PDFHandler();
        MultipartFile[] files = model.getFiles();
        System.err.println("FILES " + files);
        for(MultipartFile file : files){
            if(file.isEmpty()){
                continue;
            }
            String filename = saveUploadedFile(file);
            System.out.println(filename);
            if(filename != null){

                model = handler.getUploadModel(new File(filename));
            }
        }
        return new ResponseEntity<>(model, HttpStatus.OK);
    }


    private void indexUploadedFile(UploadModel model) throws IOException{

        MultipartFile[] files = model.getFiles();
        System.err.println("FILES " + files);
        for(MultipartFile file : files){
            if(file.isEmpty()){
                continue;
            }
            String filename = saveUploadedFile(file);
            System.out.println(filename);
            if(filename != null){
                IndexUnit indexUnit = Indexer.getInstance().getHandler(filename).getIndexUnit(new File(filename));
                indexUnit.setTitle(model.getTitle());
                indexUnit.setAuthor(model.getAuthor());
                indexUnit.setKeywords(new ArrayList<>(Arrays.asList(model.getKeywords().split(" "))));
                indexUnit.setLanguage(model.getLanguage());
                indexUnit.setCategory(model.getCategory());
                Indexer.getInstance().add(indexUnit.getLuceneDocument());
                System.err.println("Title: " + model.getTitle());
                System.err.println("Language: " + model.getLanguage());
                System.err.println("Category: " + model.getCategory() );
            }
        }
    }



    private String saveUploadedFile(MultipartFile file) throws IOException{
        String retVal = null;
        System.err.println("SAVE FILE" + file);
        if(!file.isEmpty()){
            byte[] bytes = file.getBytes();
            Path path = Paths.get(getResourceFilePath(DATA_DIR_PATH).getAbsolutePath() + File.separator + file.getOriginalFilename());
            System.err.println(File.separator);
            System.err.println(getResourceFilePath(DATA_DIR_PATH).getAbsolutePath());
            System.err.println("PATH " + path);
            Files.write(path,bytes);
            retVal = path.toString();
        }
        return retVal;
    }

    @GetMapping(value = "/download/{filename}")
    public ResponseEntity<byte[]> download(@PathVariable("filename") String filename){
        LOGGER.info("GET method, download file wit filename: " + filename);

        String path = getResourceFilePath(DATA_DIR_PATH).getAbsolutePath() + "\\" + filename + ".pdf";
        File file = null;
        try{
            file = new File(path);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        headers.add("filename",filename + ".pdf");

        byte[] bFile = readBytesFromFile(file.toString());

        return ResponseEntity.ok().headers(headers).body(bFile);
    }

    private static byte[] readBytesFromFile(String filePath){
        FileInputStream fileInputStream = null;
        byte[] bytesArray = null;
        try{
            File file = new File(filePath);
            bytesArray = new byte[(int)file.length()];

            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bytesArray);
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            if(fileInputStream != null){
                try{
                    fileInputStream.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        }

        return bytesArray;
    }



    @GetMapping(value = "/removeFile/{filename}")
    public ResponseEntity<String> removeFileHandler(@PathVariable("filename") String filename){
        LOGGER.info("GET method, remove file with filename: " + filename);
        String path = getResourceFilePath(DATA_DIR_PATH).getAbsolutePath() + "\\" + filename + ".pdf";
        System.err.println("PATH TO DELETE" + path);
        Indexer.getInstance().delete(path);
        File file = new File(path);

        if(file.delete()){
            LOGGER.info("GET method, remove file, file is deleted");
            return new ResponseEntity<>("Success", HttpStatus.OK);

        }else{
            LOGGER.info("GET method, remove file failed");
            return new ResponseEntity<>("Failed", HttpStatus.NOT_FOUND);
        }

    }

    @PutMapping(value = "/updateIndex/{filename}")
    public ResponseEntity<UploadModel> updateIndex(@RequestBody UploadModel model,@PathVariable("filename") String filename){
        LOGGER.info("PUT method, update file with filename: " + filename);

        String path = getResourceFilePath(DATA_DIR_PATH).getAbsolutePath() + "\\" + filename + ".pdf";
        IndexUnit indexUnit = Indexer.getInstance().getHandler(path).getIndexUnit(new File(path));

        indexUnit.setTitle(model.getTitle());
        indexUnit.setAuthor(model.getAuthor());
        indexUnit.setKeywords(new ArrayList<>(Arrays.asList(model.getKeywords().split(" "))));
        indexUnit.setLanguage(model.getLanguage());
        indexUnit.setCategory(model.getCategory());

        List<IndexableField> fields = new ArrayList<>(Arrays.asList(indexUnit.getLuceneDocument().getField("title"),
                indexUnit.getLuceneDocument().getField("author"),
                indexUnit.getLuceneDocument().getField("keyword"),
                indexUnit.getLuceneDocument().getField("language"),
                indexUnit.getLuceneDocument().getField("category")
        ));
        Indexer.getInstance().updateDocument(path,fields);

        return new ResponseEntity<>(model, HttpStatus.OK);
    }

}
