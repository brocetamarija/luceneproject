package ebookrepository.uns.ac.rs.ebookrepository.dto;


import ebookrepository.uns.ac.rs.ebookrepository.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class UserDTO implements Serializable {

    private Integer id;
    @NotBlank(message = "First name cannot be empty")
    @Size(min = 2,max = 30, message = "First Name should have at least 2 letter and maximum 30")
    private String first_name;
    @NotBlank(message = "Last name cannot be empty")
    @Size(min = 2,max = 30, message = "Last Name should have at least 2 letter and maximum 30")
    private String last_name;
    @NotBlank(message = "Username cannot be empty")
    @Size(min = 2,max = 30, message = "Username should have at least 2 character and maximum 10")
    private String username;
    @NotBlank(message = "Password cannot be empty")
    @Size(min = 2,max = 30, message = "Password should have at least 2 character and maximum 10")
    private String password;
    @NotBlank(message = "Authority cannot be empty")
    @Size(min = 2,max = 30, message = "Authority can have maximum 30 characters")
    private String authority;
    private CategoryDTO categoryDTO;


    public UserDTO(){
        super();
    }

    public UserDTO(Integer id, String first_name, String last_name, String username, String password, CategoryDTO categoryDTO,String authority) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.username = username;
        this.password = password;
        this.authority = authority;
        this.categoryDTO = categoryDTO;
    }

    public UserDTO(User user){
        this(user.getId(),
                user.getFirst_name(),
                user.getLast_name(),
                user.getUsername(),
                user.getPassword(),
                new CategoryDTO(user.getCategory()),
                user.getUserType());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public CategoryDTO getCategoryDTO() {
        return categoryDTO;
    }

    public void setCategoryDTO(CategoryDTO categoryDTO) {
        this.categoryDTO = categoryDTO;
    }
}
