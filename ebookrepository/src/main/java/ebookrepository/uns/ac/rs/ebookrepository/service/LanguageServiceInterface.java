package ebookrepository.uns.ac.rs.ebookrepository.service;


import ebookrepository.uns.ac.rs.ebookrepository.entity.Language;

import java.util.List;

public interface LanguageServiceInterface {

    List<Language> findAll();
    Language findOne(Integer id);
}
