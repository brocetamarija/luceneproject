package ebookrepository.uns.ac.rs.ebookrepository.controller;

import ebookrepository.uns.ac.rs.ebookrepository.entity.User;
import ebookrepository.uns.ac.rs.ebookrepository.entity.UserTokenState;
import ebookrepository.uns.ac.rs.ebookrepository.security.JwtAuthenticationRequest;
import ebookrepository.uns.ac.rs.ebookrepository.security.TokenHelper;
import ebookrepository.uns.ac.rs.ebookrepository.service.CustomUserDetailService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping(value = "api/auth")
@CrossOrigin("*")
public class AuthenticationController {

    private static final Logger LOGGER = Logger.getLogger(AuthenticationController.class);

    @Autowired
    TokenHelper tokenHelper;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    private CustomUserDetailService userDetailService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(
            @RequestBody JwtAuthenticationRequest authenticationRequest,
            HttpServletResponse response
    ) throws AuthenticationException, IOException {

        // Izvrsavanje security dela
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        authenticationRequest.getUsername(),
                        authenticationRequest.getPassword()
                )
        );

        // Ubaci username + password u kontext
        SecurityContextHolder.getContext().setAuthentication(authentication);

        // Kreiraj token
        User user = (User)authentication.getPrincipal();
        String jws = tokenHelper.generateToken( user.getUsername());

        LOGGER.info("POST method, login with username: " + user.getUsername());
        // Vrati token kao odgovor na uspesno autentifikaciju
        return ResponseEntity.ok(new UserTokenState(jws));
    }

    @RequestMapping(value = "/changePassword",method = RequestMethod.POST)
    public ResponseEntity<?> changePassword(@RequestBody PasswordChanger passwordChanger){
        LOGGER.info("POST method, change password. Old password: " + passwordChanger.oldPassword + ", New password: " + passwordChanger.newPassword);
        userDetailService.changePassword(passwordChanger.oldPassword,passwordChanger.newPassword);
        return new ResponseEntity<>("Success",HttpStatus.OK);
    }


    static class PasswordChanger {
        public String oldPassword;
        public String newPassword;
    }
}
