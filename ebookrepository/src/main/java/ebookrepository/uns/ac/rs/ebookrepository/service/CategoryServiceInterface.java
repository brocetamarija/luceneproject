package ebookrepository.uns.ac.rs.ebookrepository.service;


import ebookrepository.uns.ac.rs.ebookrepository.entity.Category;

import java.util.List;

public interface CategoryServiceInterface {

    List<Category> findAll();
    Category findOne(Integer id);
    Category findByName(String name);
    Category save(Category category);
    void remove(Integer id);
}
