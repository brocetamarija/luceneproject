package ebookrepository.uns.ac.rs.ebookrepository.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ebook")
public class EBook implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ebook_id",unique = true,nullable = false)
    private Integer id;

    @Column(name = "title",unique = true,nullable = false,length = 80)
    private String title;

    @Column(name = "author",length = 120)
    private String author;

    @Column(name = "keywords",length = 120)
    private String keywords;

    @Column(name = "publication_year")
    private Integer publication_year;

    @Column(name = "filename",unique = true,nullable = false,length = 200)
    private String filename;

    @Column(name = "MIME",length = 100)
    private String MIME;

    @ManyToOne
    @JoinColumn(name = "category_id",referencedColumnName = "category_id",nullable = false)
    private Category category;

    @ManyToOne
    @JoinColumn(name = "user_id",referencedColumnName = "user_id",nullable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "language_id",referencedColumnName = "language_id",nullable = false)
    private Language language;

    public EBook(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public Integer getPublication_year() {
        return publication_year;
    }

    public void setPublication_year(Integer publication_year) {
        this.publication_year = publication_year;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getMIME() {
        return MIME;
    }

    public void setMIME(String MIME) {
        this.MIME = MIME;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
}
