package ebookrepository.uns.ac.rs.ebookrepository.controller;


import ebookrepository.uns.ac.rs.ebookrepository.dto.CategoryDTO;
import ebookrepository.uns.ac.rs.ebookrepository.entity.Category;
import ebookrepository.uns.ac.rs.ebookrepository.service.CategoryServiceInterface;

import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value="api/category")
public class CategoryController {

    private static final Logger LOGGER = Logger.getLogger(CategoryController.class);

    @Autowired
    CategoryServiceInterface categoryService;

    @GetMapping
    public ResponseEntity<List<CategoryDTO>> getCategory(){
        LOGGER.info("GET Method, get list of categories.");
        List<Category> categories = categoryService.findAll();
        List<CategoryDTO> categoriesDTO = new ArrayList<>();
        for(Category c:categories) {
            categoriesDTO.add(new CategoryDTO(c));
        }
        return new ResponseEntity<>(categoriesDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<CategoryDTO> getCategoryById(@PathVariable("id") Integer id){
        Category category = categoryService.findOne(id);
        if(category == null){
            LOGGER.info("GET Method, category with id: " + " not found.");
            return new ResponseEntity<CategoryDTO>(HttpStatus.NOT_FOUND);
        }
        LOGGER.info("GET Method, get category with id: " + id);
        return new ResponseEntity<CategoryDTO>(new CategoryDTO(category),HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<?> saveCategory(@Validated @RequestBody CategoryDTO categoryDTO, BindingResult result){
        if(result.hasErrors()){
            List<FieldError> errors = result.getFieldErrors();
            LOGGER.info("POST Method, save category, errors: " + errors.toString());
            return new ResponseEntity<>(errors.toString(), HttpStatus.BAD_REQUEST);
        }

        Category category = new Category();

        category.setName(categoryDTO.getName());

        category = categoryService.save(category);
        LOGGER.info("POST Method, category successfully added.");
        return new ResponseEntity<>(new CategoryDTO(category),HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}",consumes = "application/json")
    public ResponseEntity<?> updateCategory(@Validated @RequestBody CategoryDTO categoryDTO, @PathVariable("id") Integer id,BindingResult result){
        if(result.hasErrors()){
            List<FieldError> errors = result.getFieldErrors();
            LOGGER.info("PUT Method, update category, errors: " + errors.toString());
            return new ResponseEntity<>(errors.toString(), HttpStatus.BAD_REQUEST);
        }

        Category category = categoryService.findOne(id);
        if(category == null) {
            LOGGER.info("PUT Method, update category, category with id: " + id + " not found.");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        category.setName(categoryDTO.getName());

        category = categoryService.save(category);
        LOGGER.info("PUT Method, update category with id: " + id);
        return new ResponseEntity<>(new CategoryDTO(category),HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deleteCategory(@PathVariable("id") Integer id){
        Category category = categoryService.findOne(id);
        if(category != null) {
            categoryService.remove(id);
            LOGGER.info("DELETE Method, delete category with id: " + id);
            return new ResponseEntity<>(HttpStatus.OK);
        }else {
            LOGGER.info("PUT Method, delete category, category with id: " + id + " not found.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
