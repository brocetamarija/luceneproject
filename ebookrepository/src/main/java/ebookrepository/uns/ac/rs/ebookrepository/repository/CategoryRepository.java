package ebookrepository.uns.ac.rs.ebookrepository.repository;

import ebookrepository.uns.ac.rs.ebookrepository.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category,Integer> {
    Category findByName(String name);
}
