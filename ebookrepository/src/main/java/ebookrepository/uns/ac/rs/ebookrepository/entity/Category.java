package ebookrepository.uns.ac.rs.ebookrepository.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "category")
public class Category implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "category_id",nullable = false,unique = true)
    private Integer category_id;

    @Column(name = "name",unique = true,nullable = false,length = 30)
    private String name;

    @OneToMany(cascade={CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="category")
    private Set<EBook> eBooks = new HashSet<>();

    public Category(){}

    public Integer getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Integer category_id) {
        this.category_id = category_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<EBook> geteBooks() {
        return eBooks;
    }

    public void seteBooks(Set<EBook> eBooks) {
        this.eBooks = eBooks;
    }
}
