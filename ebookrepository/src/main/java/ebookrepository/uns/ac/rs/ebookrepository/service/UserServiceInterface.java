package ebookrepository.uns.ac.rs.ebookrepository.service;


import ebookrepository.uns.ac.rs.ebookrepository.entity.User;

import java.util.List;

public interface UserServiceInterface {

    List<User> findAll();
    User findOne(Integer id);
    User findByUsername(String username);
    User save(User user);
    void remove(Integer id);
    User findByUsernameAndPassword(String username, String password);
}
