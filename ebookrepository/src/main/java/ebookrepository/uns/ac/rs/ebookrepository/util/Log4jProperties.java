package ebookrepository.uns.ac.rs.ebookrepository.util;

import org.apache.log4j.PropertyConfigurator;

public class Log4jProperties {

    static{
        init();
    }

    private static void init() {
        PropertyConfigurator.configure("log4j.properties");
    }
}
