package ebookrepository.uns.ac.rs.ebookrepository.model;

public final class RequiredHiglight {

    private String fieldName;
    private String value;

    public RequiredHiglight(){
        super();
    }

    public RequiredHiglight(String fieldName, String value) {
        this.fieldName = fieldName;
        this.value = value;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
