package ebookrepository.uns.ac.rs.ebookrepository.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "language")
public class Language implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "language_id",unique = true,nullable = false)
    private Integer language_id;

    @Column(name = "name",unique = true,nullable = false,length = 30)
    private String name;

    public Language(){

    }

    public Integer getLanguage_id() {
        return language_id;
    }

    public void setLanguage_id(Integer language_id) {
        this.language_id = language_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
