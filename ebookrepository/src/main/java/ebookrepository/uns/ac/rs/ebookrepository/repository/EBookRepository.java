package ebookrepository.uns.ac.rs.ebookrepository.repository;

import ebookrepository.uns.ac.rs.ebookrepository.entity.Category;
import ebookrepository.uns.ac.rs.ebookrepository.entity.EBook;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EBookRepository extends JpaRepository<EBook,Integer> {

    List<EBook> findByCategory(Category category);

    EBook findByFilename(String filename);
}
