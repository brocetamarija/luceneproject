package ebookrepository.uns.ac.rs.ebookrepository.dto;




import ebookrepository.uns.ac.rs.ebookrepository.entity.EBook;

import javax.validation.constraints.*;
import java.io.Serializable;

public class EBookDTO implements Serializable {

    private Integer id;
    @NotBlank(message = "Title cannot be empty")
    @Size(min = 2,max = 30, message = "Title should have at least 2 letter and maximum 30")
    private String title;
    @NotBlank(message = "Author cannot be empty")
    @Size(min = 2,max = 30, message = "Author should have at least 2 letter and maximum 30")
    private String author;
    @NotBlank(message = "Keywords cannot be empty")
    @Size(min = 2,max = 120, message = "Keywords should have at least 2 letter and maximum 30")
    private String keywords;
    @NotNull(message = "Year cannot be empty")
    @Min(value = 4,message ="Year must have 4 numbers")
    private Integer publication_year;
    @NotBlank(message = "Filename cannot be empty")
    private String filename;
    private String MIME;
    private CategoryDTO categoryDTO;
    private UserDTO userDTO;
    private LanguageDTO languageDTO;

    public EBookDTO(){
        super();
    }

    public EBookDTO(Integer id, String title, String author, String keywords, Integer publication_year, String filename, String MIME,
                    CategoryDTO categoryDTO, UserDTO userDTO, LanguageDTO languageDTO) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.keywords = keywords;
        this.publication_year = publication_year;
        this.filename = filename;
        this.MIME = MIME;
        this.categoryDTO = categoryDTO;
        this.userDTO = userDTO;
        this.languageDTO = languageDTO;
    }

    public EBookDTO(EBook eBook){
        this(eBook.getId(),
                eBook.getTitle(),
                eBook.getAuthor(),
                eBook.getKeywords(),
                eBook.getPublication_year(),
                eBook.getFilename(),
                eBook.getMIME(),
                new CategoryDTO(eBook.getCategory()),
                new UserDTO(eBook.getUser()),
                new LanguageDTO(eBook.getLanguage()));
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public Integer getPublication_year() {
        return publication_year;
    }

    public void setPublication_year(Integer publication_year) {
        this.publication_year = publication_year;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getMIME() {
        return MIME;
    }

    public void setMIME(String MIME) {
        this.MIME = MIME;
    }

    public CategoryDTO getCategoryDTO() {
        return categoryDTO;
    }

    public void setCategoryDTO(CategoryDTO categoryDTO) {
        this.categoryDTO = categoryDTO;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public LanguageDTO getLanguageDTO() {
        return languageDTO;
    }

    public void setLanguageDTO(LanguageDTO languageDTO) {
        this.languageDTO = languageDTO;
    }
}
