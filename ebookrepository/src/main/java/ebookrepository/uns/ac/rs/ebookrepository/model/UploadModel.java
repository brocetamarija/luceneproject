package ebookrepository.uns.ac.rs.ebookrepository.model;

import ebookrepository.uns.ac.rs.ebookrepository.entity.Language;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public class UploadModel {

    private String title;
    private String keywords;
    private String author;
    private MultipartFile[] files;
    private String filename;
    private String language;
    private int category;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public MultipartFile[] getFiles() {
        return files;
    }

    public void setFiles(MultipartFile[] files) {
        this.files = files;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }
}
