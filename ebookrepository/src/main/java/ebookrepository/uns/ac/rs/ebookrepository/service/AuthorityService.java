package ebookrepository.uns.ac.rs.ebookrepository.service;

import ebookrepository.uns.ac.rs.ebookrepository.entity.Authority;
import ebookrepository.uns.ac.rs.ebookrepository.repository.AuthorityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorityService implements AuthorityServiceInterface {

    @Autowired
    AuthorityRepository authorityRepository;

    @Override
    public Authority findByName(String name) {
        return authorityRepository.findByName(name);
    }
}
