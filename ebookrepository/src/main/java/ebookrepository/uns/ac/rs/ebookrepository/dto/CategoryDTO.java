package ebookrepository.uns.ac.rs.ebookrepository.dto;


import ebookrepository.uns.ac.rs.ebookrepository.entity.Category;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class CategoryDTO implements Serializable {

    private Integer id;
    @NotBlank(message = "Category Name cannot be empty")
    @Size(min = 2,max = 30, message = "Category Name should have at least 2 letter and maximum 30")
    private String name;

    public CategoryDTO(){
        super();
    }

    public CategoryDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public CategoryDTO(Category category){
        this(category.getCategory_id(),
                category.getName());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
