INSERT INTO category(name)VALUES('None');
INSERT INTO category(name)VALUES('Drama');
INSERT INTO category(name)VALUES('Young adult');
INSERT INTO category(name)VALUES('History fiction');

INSERT INTO users(first_name,last_name,username,user_password,user_type,category_id)VALUES('Pera','Peric','pera','$2a$04$KERxtCuhSj35hF0Gu9csjeqhSnSf68rQ68sA8xEcAZYSHknr3ixtG','admin',1);
INSERT INTO users(first_name,last_name,username,user_password,user_type,category_id)VALUES('Zika','Zikic','zika','$2a$04$KERxtCuhSj35hF0Gu9csjeqhSnSf68rQ68sA8xEcAZYSHknr3ixtG','subscriber',3);

INSERT INTO authority(name)VALUES('admin');
INSERT INTO authority(name)VALUES('subscriber');

INSERT INTO user_authority(user_id,authority_id)VALUES(1,1)
INSERT INTO user_authority(user_id,authority_id)VALUES(2,2)

INSERT INTO language(name)VALUES('English');
INSERT INTO language(name)VALUES('Serbian');
INSERT INTO language(name)VALUES('French');
INSERT INTO language(name)VALUES('German');


