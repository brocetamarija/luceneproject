$(document).ready(function(){
    sessionStorage.clear();
    $('#upload').hide();
    $('#users').hide();

    $('#loginA').click(function(e){
        document.getElementById('login').style.display='block';
    });

    $('#signUpA').click(function(e){
        document.getElementById('register').style.display='block';
    });

    var param_name = window.location.search.slice(1).split('=')[0];

    var token = localStorage.getItem("token");
    if(token != null){
        $('#loginA').replaceWith('<a id="logout"><span class="glyphicon glyphicon-log-out"></span> LogOut</a>');
        logged(token);
    }

    $('#logout').click(function(e){
        var confirmation = confirm('Are you sure you want to logout?');
        if(confirmation){
            localStorage.removeItem("token");
            localStorage.removeItem("loggedUser");
            location.reload();
        }
        e.preventDefault();
        return false;
    });

    var modal = document.getElementById('login');
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

    var modalR = document.getElementById('register');
    window.onclick = function(event) {
        if (event.target == modalR) {
            modalR.style.display = "none";
        }
    }

    getAllEBooks();
    eBooksByCategory(param_name);

    var loginBtn = $('#loginBtn');
    loginBtn.on('click',function(e){
        loginValidate();
        login();
        e.preventDefault();
        return false;
    });

    $('#registerBtn').click(function(event){
        event.preventDefault();
        registerValidate();
        register();
    });

    $('#ebooks_table').on('click','tr',function(e){
        console.log($(this).attr('data-value'));
        var id = $(this).attr('data-value');
        window.location.replace('ebook.html?id=' + id);
    });

    $('#upload').click(function(e){
        window.location.replace('upload.html');
    })

    $(document).on('ready',function(){
        registerValidate();
        loginValidate();
    });

    fillDropdown();

});

function ebooks_table_header(){
   var eBooks_table = $('#ebooks_table');
   eBooks_table.empty();
   eBooks_table.append(
        '<thead>' +
             '<tr id="header">' +
                 '<th>TITLE</th>' +
                 '<th>AUTHOR</th>' +
                 '<th>PUBLICATION YEAR</th>' +
                 '<th>CATEGORY</th>' +
                 '<th>LANGUAGE</th>' +
              '</tr>' +
        '</thead>'
   );
}

function getAllEBooks(){
    var eBooks_table = $('#ebooks_table');

    $.ajax({
        url: "http://localhost:8080/api/ebook",
        type: 'GET',
        contentType : "application/json",
        dataType:'json',
        crossDomain: true,
        success:function(response){
            console.log(response);
            if(response.length == 0){
                eBooks_table.replaceWith(
                    '<div>' +
                        '<h1>EBook Repository is empty,please come back later!</h1>' +
                    '</div>'
                );
            }else{
                eBooks_table.empty();
                ebooks_table_header();

                for(var i=0; i<response.length; i++) {
                    eBook = response[i];
                    console.log(eBook);
                    eBooks_table.append(
                        '<tr class="data" data-value="' + eBook.id +'">' +
                            '<td>' + eBook.title + '</td>' +
                            '<td>' + eBook.author + '</td>' +
                            '<td>' + eBook.publication_year + '</td>' +
                            '<td>' + eBook.categoryDTO.name + '</td>' +
                            '<td>' + eBook.languageDTO.name + '</td>' +
                        '</tr>'
                    );
                }
            }
        }
    });
}

function eBooksByCategory(param_name){
    var eBooks_table = $('#ebooks_table');
    if(param_name == 'name'){
        var category_name = window.location.search.slice(1).split('&')[0].split('=')[1];
        console.log(category_name);
        $.ajax({
            url:"http://localhost:8080/api/ebook/bycategory/" + category_name,
            type:'GET',
            contentType:'application/json',
            dataType:'json',
            crossDomain:true,
            success:function(response){
                console.log(response);
                ebooks_table_header();
                var eBooks_table = $('#ebooks_table');
                for(var i=0; i<response.length; i++) {
                    eBook = response[i];
                    console.log(eBook);
                    eBooks_table.append(
                        '<tr class="data" data-value="' + eBook.id +'">' +
                            '<td>' + eBook.title + '</td>' +
                            '<td>' + eBook.author + '</td>' +
                            '<td>' + eBook.publication_year + '</td>' +
                            '<td>' + eBook.categoryDTO.name + '</td>' +
                            '<td>' + eBook.languageDTO.name + '</td>' +
                        '</tr>'
                    );
                }
            }
        });
    }
}


function logged(token){
    $.ajax({
        url: "http://localhost:8080/api/users/logged",
        contentType:'application/json',
        dataType:'json',
        crossDomain:true,
        processData: false,
        headers: { "Authorization": "Bearer " + token},
        success:function(response){
            var loggedUser = response;
            console.log(loggedUser);
            //localStorage.setItem("loggedUser",JSON.stringify(loggedUser));
            $('#signUpA').replaceWith('<a href="user.html?user=' + loggedUser.id + '" id="user"><span>' + loggedUser.username + '</span></a>')
            if(loggedUser.authorities[0].name == "admin"){
                $('#upload').show();
                $('#users').show();
                $("#users").css("display","block");
            }
        }
    });
}

