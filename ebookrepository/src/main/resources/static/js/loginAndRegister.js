function login(){
    var username = $('#username').val().trim();
    var password = $('#password').val().trim();

    var data = {
        'username':username,
        'password':password
    }

    $.ajax({
        url:'http://localhost:8080/api/auth/login',
        type:'POST',
        contentType:'application/json',
        dataType:'json',
        data:JSON.stringify(data),
        crossDomain:true,
        success:function(response){
            console.log(response);
            var token = response.access_token;
            console.log(token);
            localStorage.setItem("token",token);
            alert("You have been successfully logged in!");
            document.getElementById('login').style.display='none'
            $('#loginA').replaceWith('<a id="logout"><span class="glyphicon glyphicon-log-out"></span> LogOut</a>');
            logged(token);
            location.reload();
        },
        error:function(response){
            console.log(response);
            alert("Username or password are wrong,please try again!");
        }
    });
}

function register(){
    var username = $('#usernameR').val().trim();
    var password = $('#passwordR').val().trim();
    var cpassword = $('#cpassword').val().trim();
    var fname = $('#fname').val().trim();
    var lname = $('#lname').val().trim();
    var category = $('#dropdown').val();

    if(password != cpassword){
        alert("Password do not match!")
    }else{

        data = {
            'first_name':fname,
            'last_name':lname,
            'username':username,
            'password':password,
            'authority':'subscriber',
            'categoryDTO': {
                'id': category
            }
        }

        $.ajax({
            type:'POST',
            url:'http://localhost:8080/api/users',
            contentType:'application/json',
            dataType:'json',
            crossDomain:true,
            processData: false,
            data:JSON.stringify(data),
            success:function(response){
                console.log("Added user!");
                console.log(response);
                document.getElementById('register').style.display='none';
                alert("You have been successfully registered!");
                document.getElementById('login').style.display='block';
            },
            error:function(response){
                console.log(response);
            }
        });
    }
}

function fillDropdown(){
    var dropdown = $('#dropdown');
    console.log(dropdown);

    $.ajax({
        type:'GET',
        url: "http://localhost:8080/api/category",
        contentType : "application/json",
        dataType:'json',
        crossDomain: true,
        success:function(response){
            for(var i = 0; i < response.length; i++) {
               category = response[i];
               dropdown.append('<option value='+category.id+'>'+category.name+'</option>');
            }
        }
    });
}

function loginValidate(){
    var username = $('#username').val().trim();
    var password = $('#password').val().trim();

    $('#loginForm').validate({
        onfocusout:function(element){
            console.log(element);
            $(element).valid();
        },
        rules:{
            username:{
                required:true,
                minlength:2,
                maxlength:10
            },
            password:{
                required:true,
                minlength:3,
                maxlength:15
            }
        },
        messages:{
            username:{
                required:"Please enter your username",
                minlength:"Must enter at least 2 characters",
                maxlength:"Maximum number of characters is 30"
            },
            password:{
                required:"Please enter your password",
                minlength:"Must enter at least 3 characters",
                maxlength:"Maximum number of characters is 15"
            }
        }
    });
}

function registerValidate(){
    var username = $('#usernameR').val();
    var password = $('#passwordR').val();
    var cpassword = $('#cpassword').val();
    var first_name = $('#fname').val();
    var last_name = $('#lname').val();

    console.log("Validacija");
    $('#registerForm').validate({
        onfocusout:function(element){
            console.log(element);
            $(element).valid();
        },
        rules:{
            first_name:{
                required:true,
                minlength:2,
                maxlength:30

            },
            last_name:{
                required:true,
                minlength:2,
                maxlength:30

            },
            username:{
                required:true,
                minlength:2,
                maxlength:10
            },
            password:{
                required:true,
                minlength:3,
                maxlength:15
            },
            cpassword:{
                required:true,
                minlength:3,
                maxlength:15,
                equalTo:"#passwordR"
            }
        },
        messages:{
            first_name:{
                required:"Please enter your name",
                minlength:"Must enter at least 2 characters",
                maxlength:"Maximum number of characters is 30"

            },
            last_name:{
                required:"Please enter your last name",
                minlength:"Must enter at least 2 characters",
                maxlength:"Maximum number of characters is 30"

            },
            username:{
                required:"Please enter your username",
                minlength:"Must enter at least 2 characters",
                maxlength:"Maximum number of characters is 30"
            },
            password:{
                required:"Please enter your password",
                minlength:"Must enter at least 3 characters",
                maxlength:"Maximum number of characters is 15"
            },
            cpassword:{
                required:"Please confirm password",
                minlength:"Must enter at least 3 characters",
                maxlength:"Maximum number of characters is 15",
                equalTo:"Please enter the same password"
            }
        }
    })
}
