$(document).ready(function(){
    sessionStorage.clear();

    var userId = 0;
    $('.overlay').hide();
    let user = JSON.parse(localStorage.getItem('loggedUser'));
    console.log(user);
    var token = localStorage.getItem("token");
    console.log(token);
    if(token != null){
        $('#loginA').replaceWith('<a id="logout"><span class="glyphicon glyphicon-log-out"></span> LogOut</a>');
        logged(token);
    }

    if(token == null){
        $('.container').hide();
        $('.overlay').show();
    }

    $('#errorMessage').hide();
    $('#users').hide();
    $('#btnAdd').hide();

    $('#loginA').click(function(e){
        document.getElementById('login').style.display='block';
    });

    $('#signUpA').click(function(e){
        document.getElementById('register').style.display='block';
    });

    $('#logout').click(function(e){
        var confirmation = confirm('Are you sure you want to logout?');
        if(confirmation){
            localStorage.removeItem("token");
            localStorage.removeItem("loggedUser");
            window.location.replace("index.html");
        }
        e.preventDefault();
        return false;
    });

    if(token != null || user != null){
        if(user.authorities[0].name == 'admin'){
            getUsers(token);
        }
    }

    $("#submitAdd").click(function (event) {
        event.preventDefault();
        $('#addForm').valid();
        if(userId == 0){
            addUser(token);
        }else{
            updateUser(userId,token);
        }
    });

    $('#btnAdd').click(function(event){
        event.preventDefault();
        $('input[name=firstName]').val('');
        $('input[name=lastName]').val('');
        $('input[name=addUsername]').val('');
        $('input[name=addPassword]').val('');
        $("#userType").val("admin");
        userId = 0;
        $('input[name=addPassword]').show();
        $('#passLbl').show();
    });

    $("#users_table").on('click','td#updateTd',function(e) {
        userId = $(this).attr('name');
        console.log(userId);
        findById(userId,token);
    });

    $("#users_table").on('click','td#deleteTd',function(e) {
        userId = $(this).attr('name');
        console.log(userId);
        var confirmation = confirm('Are you sure you want to delete user?');
        if(confirmation){
            deleteUser(userId,token);
            location.reload();
        }

    });
    $(document).on('ready',function(){
        addUserValidate();
        registerValidate();
    });

    fillDropdown();

});

function getUsers(token){
console.log("GET");
    var loggedUser = JSON.parse(localStorage.getItem('loggedUser'));

    $.ajax({
         url: "http://localhost:8080/api/users",
         type: 'GET',
         contentType : "application/json",
         headers: { "Authorization": "Bearer " + token},
         dataType:'json',
         crossDomain: true,
         success:function(response){
            var users_table = $('#users_table');
            users_table.empty();
            users_tableHeader();

            console.log(response);
            for(var i=0; i<response.length; i++) {

                user = response[i];
                users_table.append(
                    '<tr class="data">' +
                        '<td>' + user.first_name + '</td>' +
                        '<td>' + user.last_name + '</td>' +
                        '<td>' + user.username + '</td>' +
                        '<td>' + user.authority + '</td>' +
                        '<td>' + user.categoryDTO.name + '</td>' +
                        '<td id="updateTd" name="' + user.id + '"><button id="update" name="' + user.id + '" class="btn btn-default">Update</button></td>' +
                        '<td id="deleteTd" name="' + user.id + '"><button id="' + user.id + '" name="' + user.id + '" class="btn btn-default btnDelete">Delete</button></td>' +
                    '</tr>'

                );

                var btnDelete = 'button#' + loggedUser.id;
                console.log(btnDelete);
                $(btnDelete).hide();

            }
         }
    });
}

function users_tableHeader(){
    var users_table = $('#users_table');
    users_table.empty();
    users_table.append(
        '<thead>' +
            '<tr>' +
                '<th>First name</th>' +
                '<th>Last name</th>' +
                '<th>Username</th>' +
                '<th>User Type</th>' +
                '<th>Category</th>' +
                '<th>Update</th>' +
                '<th>Delete</th>' +
            '</tr>' +
        '</thead>'
    );
}


function addUser(token){
    var first_name = $('input[name=firstName]').val();
    var last_name = $('input[name=lastName]').val();
    var username = $('input[name=addUsername]').val();
    var password = $('input[name=addPassword]').val();
    var userType = $("#userType").val();
    var category = $('#dropdown').val();

    data = {
        'first_name':first_name,
        'last_name':last_name,
        'username':username,
        'password':password,
        'authority':userType,
        'categoryDTO': {
            'id': category
        }
    }

    $.ajax({
        type:'POST',
        url:'http://localhost:8080/api/users',
        contentType:'application/json',
        dataType:'json',
        headers: { "Authorization": "Bearer " + token},
        crossDomain:true,
        processData: false,
        data:JSON.stringify(data),
        success:function(response){
            console.log(response);
            document.getElementById('add').style.display='none';
            getUsers(token);
        },
        error: function(response) {
            if(response.status == 400){
                $('#errorMessage').show();
            }
        }
    });
}

function findById(id,token){
    $.ajax({
        type:'GET',
        url:'http://localhost:8080/api/users/' + id,
        contentType:'application/json',
        headers: { "Authorization": "Bearer " + token},
        dataType:'json',
        crossDomain:true,
        success:function(response){
            console.log(response);
            user = response;
            var first_name = $('input[name=firstName]').val(user.first_name);
            var last_name = $('input[name=lastName]').val(user.last_name);
            var username = $('input[name=addUsername]').val(user.username);
            var password = $('input[name=addPassword]').val(user.password);
            var userType = $("#userType").val(user.authority).change();
            var category = $("#dropdown").val(user.categoryDTO.id).change();
            var modalAdd = document.getElementById('add');
            modalAdd.style.display = "block";
            $('input[name=addPassword]').hide();
            $('#passLbl').hide();

        }
    });
}

function updateUser(id,token){
    var first_name = $('input[name=firstName]').val();
    var last_name = $('input[name=lastName]').val();
    var username = $('input[name=addUsername]').val();
    var password = $('input[name=addPassword]').val();
    var userType = $("#userType").val();
    var category = $('#dropdown').val();
    console.log(userType);

    var data = {
        'first_name':first_name,
        'last_name':last_name,
        'username':username,
        'password':password,
        'authority':userType,
        'authorities':[
            {

                "name": userType,

            }
        ],
        'categoryDTO': {
            'id': category
        }
    }

    $.ajax({
        type:'PUT',
        headers: { "Authorization": "Bearer " + token},
        url:'http://localhost:8080/api/users/' + id + '/' + userType,
        data:JSON.stringify(data),
        contentType:'application/json',
        dataType:'json',
        crossDomain:true,
        success:function(response){
            console.log(response);
            document.getElementById('add').style.display='none';
            logged(token);
            getUsers(token);

        },
        error: function(response) {
            if(response.status == 400){
                console.log(response);
                $('#errorMessage').show();
            }
        }
    });
}

function deleteUser(id,token){
console.log("DELETE");
    $.ajax({
        url:'http://localhost:8080/api/users/' + id,
        type:'DELETE',
        headers: { "Authorization": "Bearer " + token},
        contentType:'application/json',
        dataType:'json',
        crossDomain:true,
        success:function(response){

        }
    });

}

function logged(token){

    $.ajax({
        url: "http://localhost:8080/api/users/logged",
        contentType:'application/json',
        dataType:'json',
        crossDomain:true,
        processData: false,
        headers: { "Authorization": "Bearer " + token},
        success:function(response){
            var loggedUser = response;

            console.log(loggedUser);
            localStorage.setItem("loggedUser",JSON.stringify(loggedUser));
            $('#signUpA').replaceWith('<a href="user.html?user=' + loggedUser.id + '" id="user"><span>' + loggedUser.username + '</span></a>')
            if(loggedUser.authorities[0].name == "admin"){
                $('#users').show();
                $('#btnAdd').show();
            }

        }
    })
}

function addUserValidate(){
    var first_name = $('input[name=firstName]').val();
    var last_name = $('input[name=lastName]').val();
    var username = $('input[name=addUsername]').val();
    var password = $('input[name=addPassword]').val();
    console.log("Validacija");
    $('#addForm').validate({
        onfocusout:function(element){
            console.log(element);
            $(element).valid();
        },
        rules:{
            first_name:{
                required:true,
                minlength:2,
                maxlength:30

            },
            last_name:{
                required:true,
                minlength:2,
                maxlength:30

            },
            username:{
                required:true,
                minlength:2,
                maxlength:10
            },
            password:{
                required:true,
                minlength:3,
                maxlength:15
            }
        },
        messages:{
            first_name:{
                required:"Please enter your name",
                minlength:"Must enter at least 2 characters",
                maxlength:"Maximum number of characters is 30"

            },
            last_name:{
                required:"Please enter your last name",
                minlength:"Must enter at least 2 characters",
                maxlength:"Maximum number of characters is 30"

            },
            username:{
                required:"Please enter your username",
                minlength:"Must enter at least 2 characters",
                maxlength:"Maximum number of characters is 30"
            },
            password:{
                required:"Please enter your password",
                minlength:"Must enter at least 3 characters",
                maxlength:"Maximum number of characters is 15"
            }
        }
    });
}
