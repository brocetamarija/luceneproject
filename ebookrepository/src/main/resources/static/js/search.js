$(document).ready(function(){
    sessionStorage.clear();
    var token = localStorage.getItem("token");
    var searchType = $('#searchType');

    $('#users').hide();

    var filename = '';

    $('#loginA').click(function(e){
        document.getElementById('login').style.display='block';
    });

    $('#signUpA').click(function(e){
        document.getElementById('register').style.display='block';
    });

    if(token != null){
        $('#loginA').replaceWith('<a id="logout"><span class="glyphicon glyphicon-log-out"></span> LogOut</a>');
        logged(token);
    }

    $('#logout').click(function(e){
        var confirmation = confirm('Are you sure you want to logout?');
        if(confirmation){
            localStorage.removeItem("token");
            localStorage.removeItem("loggedUser");
            $('#logout').replaceWith('<a class="active" id="loginA">Login</a>');
            $('#user').replaceWith('<a class="active" id="signUpA">Sign Up</a>');
            $('#users').hide();
            location.reload();
        }
        e.preventDefault();
        return false;
    });

    var modal = document.getElementById('login');
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

    var modalR = document.getElementById('register');
    window.onclick = function(event) {
        if (event.target == modalR) {
            modalR.style.display = "none";
        }
    }

    var loginBtn = $('#loginBtn');
    loginBtn.on('click',function(e){
        loginValidate();
        login();
        e.preventDefault();
        return false;
    });

    $('#registerBtn').click(function(event){
        event.preventDefault();
        registerValidate();
        register();
    });


    $('#booleanSearch').hide();
    $('#field2').hide();
    searchType.change(function(){
        var searchType = $('#searchType').val();
        console.log(searchType);
        if(searchType == 'boolean'){
            $('#form').hide();
            $('#field2').show();
            $('#booleanSearch').show();
        }else{
            $('#field2').hide();
            $('#booleanSearch').hide();
            $('#form').show();
        }
    });

    var searchType = $('#searchType').val();
    console.log(searchType);


    $('#btnSubmitBoolean').click(function(e){
        event.preventDefault();
        booleanSearch(token);
    });

    $("#btnSubmitLuceneQueryLanguage").click(function (event) {
        //stop submit the form, we will post it manually.
        event.preventDefault();

        var searchType = $('#searchType').val();
        console.log(searchType);
        if(searchType == 'regular'){
            regularSearch(token);
        }else if(searchType == 'phrase'){
            phraseSearch(token);
        }else if(searchType == 'fuzzy'){
            fuzzySearch(token);
        }

    });

    $('#resultTable').on('click','td#notBtn',function(e){
        e.preventDefault();
        console.log("not btn");
        console.log($(this).attr('data-value'));
        var filename = $(this).attr('data-value');
        console.log(filename);
        window.location.replace('ebook.html?filename=' + filename);
    });

    $('#resultTable').on('click','td#btnDownload',function(e){
        e.preventDefault();
        console.log('btn');
        console.log('download');
        download(token);
    });

    $('#downloadLoginBtn').click(function(e){
        document.getElementById('options').style.display='none';
        document.getElementById('login').style.display='block';
    });

    $('#downloadRegisterBtn').click(function(e){
        document.getElementById('options').style.display='none';
        document.getElementById('register').style.display='block';
    });

    $('#downloadCatBtn').click(function(e){
        window.location.replace('category.html');
    });

    $(document).on('ready',function(){

        registerValidate();
        loginValidate();
    });

    $('#resultTable td:nth-last-child(1),th:nth-last-child(1)').hide();

    fillDropdown();

});

function clickOnTD(file){
    console.log("tr");
    console.log("file");
    window.location.replace('ebook.html?filename=' + file);
}

function resultTableHeader(){
    var resultTable = $('#resultTable');
    resultTable.empty();
    resultTable.append(
        '<thead>' +
             '<tr id="header">' +
                 '<th>Title</th>' +
                 '<th>Author</th>' +
                 '<th>Keywords</th>' +
                 '<th>Highlight</th>' +
                 '<th>Download</th>' +
              '</tr>' +
        '</thead>'
    );
}

function regularSearch(token){
    var user = JSON.parse(localStorage.getItem('loggedUser'));
    var value = $('#luceneQueryLanguage input[name=value]').val();
    var field = $('#field').val();
    var resultTable = $('#resultTable');
    console.log(value);
    console.log(field);

    var data = {
        'value':value,
        'field':field
    }

    $.ajax({
        type: "POST",
        url: "api/search/regular",
        data: JSON.stringify(data),
        contentType: 'application/json',
        success: function (data) {

            console.log("Data length " + data.length);

            if(data.length == 0){
                $('#resultDivTable').append('<h3>No results</h3>');
                resultTable.hide();
            }else{
                resultTable.show();
                $('#resultDivTable h3').hide();
                resultTable.empty();
                resultTableHeader();

                for(i = 0; i < data.length; i++){

                    var result = data[i];
                    filename = result.location;
                    var splitted = filename.split('files\\');
                    console.log(splitted[1]);
                    var splitted1 = splitted[1].split(".");
                    console.log(splitted1[0]);
                    var file = splitted1[0];

                    resultTable.append(
                        '<tr class="data" data-value="' + file +'">' +
                            '<td id="notBtn" data-value="' + file +'">' + result.title + '</td>' +
                            '<td id="notBtn" data-value="' + file +'">' + result.author + '</td>' +
                            '<td id="notBtn" data-value="' + file +'">' + result.keywords + '</td>' +
                            '<td id="notBtn" data-value="' + file +'">' + result.higlight + '</td>' +
                            '<td class="download" id="btnDownload" name="' + file + '"><button id="' + file + '" class="btn btn-default">Download</button></td>' +
                        '</tr>'
                    );

                    if(user != null){
                        if(user.category.category_id == 1 || user.category.category_id == result.category){
                            $('#resultTable td:nth-last-child(1),th:nth-last-child(1)').show();
                        }else{
                            $('#resultTable td:nth-last-child(1),th:nth-last-child(1)').hide();
                            document.getElementById('goToCat').style.display='block';
                        }
                    }else{
                        $('#resultTable td:nth-last-child(1),th:nth-last-child(1)').hide();
                        document.getElementById('options').style.display='block';
                    }
                    console.log("SUCCESS : ", data);
                    $("#btnSubmitLuceneQueryLanguage").prop("disabled", false);
                }
            }
        },
        error: function (e) {
            console.log("ERROR : ", e);
            $("#btnSubmitLuceneQueryLanguage").prop("disabled", false);
        }
    });

}

function phraseSearch(token){
    var user = JSON.parse(localStorage.getItem('loggedUser'));
    var value = $('#luceneQueryLanguage input[name=value]').val();
    var field = $('#field').val();
    var resultTable = $('#resultTable');
    console.log(value);
    console.log(field);

    var data = {
        'value':value,
        'field':field
    }

    $.ajax({
        type: "POST",
        url: "api/search/phrase",
        data: JSON.stringify(data),
        contentType: 'application/json',
        success: function (data) {
            if(data.length == 0){
                $('#resultDivTable').append('<h3>No results</h3>');
                resultTable.hide();
            }else{
                resultTable.show();
                $('#resultDivTable h3').hide();
                resultTable.empty();
                resultTableHeader();
                for(i = 0; i < data.length; i++){
                    var result = data[i];

                    filename = result.location;
                    var splitted = filename.split('files\\');
                    console.log(splitted[1]);
                    var splitted1 = splitted[1].split(".");
                    console.log(splitted1[0]);
                    var file = splitted1[0];

                    resultTable.append(
                            '<tr class="data" data-value="' + file +'">' +
                                '<td id="notBtn" data-value="' + file +'">' + result.title + '</td>' +
                                '<td id="notBtn" data-value="' + file +'">' + result.author + '</td>' +
                                '<td id="notBtn" data-value="' + file +'">' + result.keywords + '</td>' +
                                '<td id="notBtn" data-value="' + file +'">' + result.higlight + '</td>' +
                                '<td class="download" id="btnDownload" name="' + file + '"><button id="' + file + '" class="btn btn-default">Download</button></td>' +
                            '</tr>'
                    );

                    if(user != null){
                        if(user.category.category_id == 1 || user.category.category_id == result.category){
                           $('#resultTable td:nth-last-child(1),th:nth-last-child(1)').show();
                        }else{
                            $('#resultTable td:nth-last-child(1),th:nth-last-child(1)').hide();
                            document.getElementById('goToCat').style.display='block';
                        }
                    }else{
                        $('#resultTable td:nth-last-child(1),th:nth-last-child(1)').hide();
                        document.getElementById('options').style.display='block';
                    }
                }
                console.log("SUCCESS : ", data);
                $("#btnSubmitLuceneQueryLanguage").prop("disabled", false);
            }
        },
        error: function (e) {
            console.log("ERROR : ", e);
            $("#btnSubmitLuceneQueryLanguage").prop("disabled", false);
        }
    });
}

function fuzzySearch(token){
    var user = JSON.parse(localStorage.getItem('loggedUser'));
    var value = $('#luceneQueryLanguage input[name=value]').val();
    var field = $('#field').val();
    var resultTable = $('#resultTable');
    console.log(value);
    console.log(field);

    var data = {
        'value':value,
        'field':field
    }

    $.ajax({
        type: "POST",
        url: "api/search/fuzzy",
        data: JSON.stringify(data),

        contentType: 'application/json',
        success: function (data) {
            if(data.length == 0){
                $('#resultDivTable').append('<h3>No results</h3>');
                resultTable.hide();
            }else{
                resultTable.show();
                $('#resultDivTable h3').hide();
                resultTable.empty();
                resultTableHeader();
                for(i = 0; i < data.length; i++){
                    var result = data[i];
                    filename = result.location;
                    var splitted = filename.split('files\\');
                    console.log(splitted[1]);
                    var splitted1 = splitted[1].split(".");
                    console.log(splitted1[0]);
                    var file = splitted1[0];

                    resultTable.append(
                            '<tr class="data" data-value="' + file +'">' +
                                '<td id="notBtn" data-value="' + file +'">' + result.title + '</td>' +
                                '<td id="notBtn" data-value="' + file +'">' + result.author + '</td>' +
                                '<td id="notBtn" data-value="' + file +'">' + result.keywords + '</td>' +
                                '<td id="notBtn" data-value="' + file +'">' + result.higlight + '</td>' +
                                '<td class="download" id="btnDownload" name="' + file + '"><button id="' + file + '" class="btn btn-default">Download</button></td>' +
                            '</tr>'
                    );

                    if(user != null){
                        if(user.category.category_id == 1 || user.category.category_id == result.category){
                           $('#resultTable td:nth-last-child(1),th:nth-last-child(1)').show();
                        }else{
                            $('#resultTable td:nth-last-child(1),th:nth-last-child(1)').hide();
                            document.getElementById('goToCat').style.display='block';
                        }
                    }else{
                        $('#resultTable td:nth-last-child(1),th:nth-last-child(1)').hide();
                        document.getElementById('options').style.display='block';
                    }
                }
                console.log("SUCCESS : ", data);
                $("#btnSubmitLuceneQueryLanguage").prop("disabled", false);
            }
        },
        error: function (e) {
            console.log("ERROR : ", e);
            $("#btnSubmitLuceneQueryLanguage").prop("disabled", false);
        }
    });
}

function booleanSearch(token){
    var user = JSON.parse(localStorage.getItem('loggedUser'));
    var value1 = $('#booleanForm input[name=value1]').val();
    var value2 = $('#booleanForm input[name=value2]').val();
    var field1 = $('#field').val();
    var field2 = $('#field2').val();
    var operation = $('#operation').val();
    var resultTable = $('#resultTable');

    var data = {
        'field1':field1,
        'value1':value1,
        'field2':field2,
        'value2':value2,
        'operation':operation
    }

    $.ajax({
        type: "POST",
        url: "api/search/boolean",
        data: JSON.stringify(data),

        contentType: 'application/json',
        success: function (data) {
            if(data.length == 0){
                $('#resultDivTable').append('<h3>No results</h3>');
                resultTable.hide();
            }else{
                resultTable.show();
                $('#resultDivTable h3').hide();
                resultTable.empty();
                resultTableHeader();
                for(i = 0; i < data.length; i++){
                    var result = data[i];

                    filename = result.location;
                    var splitted = filename.split('files\\');
                    console.log(splitted[1]);
                    var splitted1 = splitted[1].split(".");
                    console.log(splitted1[0]);
                    var file = splitted1[0];

                    resultTable.append(
                            '<tr class="data" data-value="' + file +'">' +
                                '<td id="notBtn" data-value="' + file +'">' + result.title + '</td>' +
                                '<td id="notBtn" data-value="' + file +'">' + result.author + '</td>' +
                                '<td id="notBtn" data-value="' + file +'">' + result.keywords + '</td>' +
                                '<td id="notBtn" data-value="' + file +'">' + result.higlight + '</td>' +
                                '<td class="download" id="btnDownload" name="' + file + '"><button id="' + file + '" class="btn btn-default">Download</button></td>' +
                            '</tr>'
                    );

                    if(user != null){
                        if(user.category.category_id == 1 || user.category.category_id == result.category){
                           $('#resultTable td:nth-last-child(1),th:nth-last-child(1)').show();
                        }else{
                            $('#resultTable td:nth-last-child(1),th:nth-last-child(1)').hide();
                            document.getElementById('goToCat').style.display='block';
                        }
                    }else{
                        $('#resultTable td:nth-last-child(1),th:nth-last-child(1)').hide();
                        document.getElementById('options').style.display='block';
                    }
                }
                console.log("SUCCESS : ", data);
                $("#btnSubmitBoolean").prop("disabled", false);
            }
        },
        error: function (e) {
            console.log("ERROR : ", e);
            $("#btnSubmitBoolean").prop("disabled", false);
        }
    });
}

function logged(token){
    $.ajax({
        url: "http://localhost:8080/api/users/logged",
        contentType:'application/json',
        dataType:'json',
        crossDomain:true,
        processData: false,
        headers: { "Authorization": "Bearer " + token},
        success:function(response){
            var loggedUser = response;
            console.log(loggedUser);
            localStorage.setItem("loggedUser",JSON.stringify(loggedUser));
            $('#signUpA').replaceWith('<a href="user.html?user=' + loggedUser.id + '" id="user"><span>' + loggedUser.username + '</span></a>')
            if(loggedUser.authorities[0].name == "admin"){
                $('#users').show();
                $("#users").css("display","block");
            }
        }
    })
}

function download(token){
    var splitted = filename.split('files\\');
    console.log(splitted[1]);
    var splitted1 = splitted[1].split(".");
    console.log(splitted1[0]);
    var file = splitted1[0];
    var xhr = new XMLHttpRequest();
    xhr.open('GET', "/api/indexer/download/" + file, true);
    xhr.setRequestHeader("Authorization", "Bearer " + token);
    xhr.responseType = 'blob';

    xhr.onload = function(e){
        if(this.status == 200){
            var blob = this.response;
            console.log(blob);
            var a = document.createElement('a');
            var url = window.URL.createObjectURL(blob);
            a.href = url;
            a.download = xhr.getResponseHeader('filename');
            a.click();
            window.URL.revokeObjectURL(url);
        }
    };

    xhr.send();
}

