$(document).ready(function(){
    sessionStorage.clear();
    var categoryId = 0;

    var token = localStorage.getItem("token");
    if(token != null){
        logged(token);
    }

    let user = JSON.parse(localStorage.getItem('loggedUser'));
    console.log(user);

    $('#errorMessage').hide();
    $('#users').hide();
    $('#btnAdd').hide();

    $('#loginA').click(function(e){
        document.getElementById('login').style.display='block';
    });

    $('#signUpA').click(function(e){
        document.getElementById('register').style.display='block';
    });

    if(token != null){
        $('#loginA').replaceWith('<a id="logout"><span class="glyphicon glyphicon-log-out"></span> LogOut</a>')
    }

    $('#logout').click(function(e){
        var confirmation = confirm('Are you sure you want to logout?');
        if(confirmation){
            localStorage.removeItem("token");
            localStorage.removeItem("loggedUser");
            location.reload();
        }
        e.preventDefault();
        return false;
    });


    var modal = document.getElementById('login');
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

    var modal = document.getElementById('register');
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

    var modalAdd = document.getElementById('add');
    window.onclick = function(event) {
        if (event.target == modalAdd) {
            modalAdd.style.display = "none";
        }
    }

     $("#category_table").on('click','td#name',function(e) {

        console.log($(this).text());
        var name = $(this).text();
        window.location.replace("index.html?name=" + name);
     });

    $("#submitAdd").click(function (event) {
        event.preventDefault();
        addCategoryValidate();
        console.log(categoryId);
        if(categoryId == 0){
            addCategory(token);
        }else{
            updateCategory(categoryId,token);
        }

    });

    $("#category_table").on('click','td.update',function(e) {
            categoryId = $(this).attr('name');
            console.log(categoryId);
            findById(categoryId,token);
    });

    $("#category_table").on('click','td.subscribe',function(e) {
            categoryId = $(this).attr('name');
            if(user != null){
                if(user.category.category_id == categoryId){
                    console.log("Unsubscribe");
                    unsubscribe(user.id,token);
                }else{
                    console.log("Subscribe");
                    subscribe(user.id,categoryId,token);
                }
            }
    });

    $("#category_table").on('click','td.delete',function(e) {
            categoryId = $(this).attr('name');
            categoryName = $(this).attr('id');
            console.log(categoryName);
            findEBooksByCategory(categoryName,token);
            console.log(categoryId);
            var confirmation = confirm('Are you sure you want to delete category?');
            if(confirmation){
                deleteCategory(categoryId,token);
                //location.reload();
            }

    });

    $('#btnAdd').click(function(event){
        event.preventDefault();
        $('#addInput').val('');
        categoryId = 0;
    });

    $('#loginBtn').on('click',function(e){
        e.preventDefault();
        loginValidate();
        login();
    });

    $('#registerBtn').click(function(event){
        event.preventDefault();
        registerValidate();
        register();
    });

    $(document).on('ready',function(){
        addCategoryValidate();
        registerValidate();
        loginValidate();

    });

    window.onload = getCategories;

    fillDropdown();
});

function getCategories(){
    var user = JSON.parse(localStorage.getItem('loggedUser'));
    console.log("GET");
    console.log(user);
    var header = $('#header');


    $.ajax({
        url: "http://localhost:8080/api/category",
        type: 'GET',
        contentType : "application/json",
        dataType:'json',
        crossDomain: true,
        success:function(response){
            console.log(response);
            var category_table = $('#category_table');
            category_table.empty();
            category_table_header();
            if(user != null){
                 if(user.authorities[0].name == "admin"){
                     $('#header').append($('<th />', {text : 'UPDATE'}))
                     $('#header').append($('<th />', {text : 'DELETE'}))
                 }else if(user.authorities[0].name == "subscriber"){
                    $('#header').append($('<th />', {text : 'SUBSCRIBE'}))
                 }
            }

            for(var i=1; i<response.length; i++) {
                category = response[i];
                console.log(category);

                if(user == null){

                     category_table.append(
                         '<tr class="data" data-value="' + category.name +'">' +
                             '<td id="name">' + category.name + '</td>' +
                         '</tr>'
                     );
                }else{

                    if(user.authorities[0].name == "admin"){

                         category_table.append(
                             '<tr class="data" data-value="' + category.name +'">' +
                                 '<td id="name">' + category.name + '</td>' +
                                 '<td class="update" name="' + category.id + '"><button id="btnUpdate" class="btn btn-default">Update</button></td>' +
                                 '<td class="delete" name="' + category.id + '" id="' + category.name + '"><button id="btnDelete" class="btn btn-default">Delete</button></td>' +
                             '</tr>'
                         );
                    }
                    if(user.authorities[0].name == "subscriber"){
                         category_table.append(
                             '<tr class="data" data-value="' + category.name +'">' +
                                 '<td id="name">' + category.name + '</td>' +
                                 '<td class="subscribe" name="' + category.id + '">' +
                                    '<button id="' + category.id + 'subscribe" class="btn btn-default subscribeBtn">Subscribe</button>' +
                                    '<button id="' + category.id + 'unsubscribe" class="btn btn-default unsubscribe">Unsubscribe</button>' +
                                 '</td>' +
                             '</tr>'
                         );


                          let btnCatSub = 'button#' + user.category.category_id + 'subscribe';
                          $(btnCatSub).hide();

                          $('.unsubscribe').hide();

                          let btnCatUnSub = 'button#' + user.category.category_id + 'unsubscribe';
                          $(btnCatUnSub).show();

                    }
                }
            }
        }
    });
}


function category_table_header(){
   var category_table = $('#category_table');
   category_table.empty();
   category_table.append(
    '<thead>' +
         '<tr id="header">' +
             '<th>NAME</th>' +
          '</tr>' +
    '</thead>'
   );
}

function addCategory(token){
    var name = $('input[name=addCategory]').val();

    var data = {
        "name":name
    }

    $.ajax({
        type:'POST',
        url:'http://localhost:8080/api/category',
        contentType:'application/json',
        headers: { "Authorization": "Bearer " + token},
        data:JSON.stringify(data),
        dataType:'json',
        crossDomain:true,
        success:function(response){
            console.log(response);
            document.getElementById('add').style.display='none';
            getCategories();
        },
       error: function(response) {
            if(response.status == 400){
                $('#errorMessage').show();
            }
        }
    });
}

function updateCategory(id,token){

    var data = {
        "name": $('#addInput').val()
    }

    $.ajax({
        type:'PUT',
        url:'http://localhost:8080/api/category/' + id,
        data:JSON.stringify(data),
        headers: { "Authorization": "Bearer " + token},
        contentType:'application/json',
        dataType:'json',
        crossDomain:true,
        success:function(response){
            document.getElementById('add').style.display='none';
            getCategories();
        },
        error:function(response){
            if(response.status == 400){
                $('#errorMessage').show();
            }
        }
    });
}

function deleteCategory(id,token){
    $.ajax({
        url:'http://localhost:8080/api/category/' + id,
        type:'DELETE',
        headers: { "Authorization": "Bearer " + token},
        contentType:'application/json',
        dataType:'json',
        crossDomain:true,
        success:function(response){

        }
    });

}

function findById(id,token){
    $.ajax({
        type:'GET',
        url:'http://localhost:8080/api/category/' + id,
        headers: { "Authorization": "Bearer " + token},
        contentType:'application/json',
        dataType:'json',
        crossDomain:true,
        success:function(response){
            console.log(response);

            $('#addInput').val(response.name);

            var modalAdd = document.getElementById('add');
            modalAdd.style.display = "block";
        }
    });
}

function logged(token){

    $.ajax({
        url: "http://localhost:8080/api/users/logged",
        contentType:'application/json',
        dataType:'json',
        crossDomain:true,
        processData: false,
        headers: { "Authorization": "Bearer " + token},
        success:function(response){
            var loggedUser = response;
            console.log(loggedUser.category.category_id + " Category");
            console.log(loggedUser);
            localStorage.setItem("loggedUser",JSON.stringify(loggedUser));
            $('#signUpA').replaceWith('<a href="user.html?user=' + loggedUser.id + '" id="user"><span>' + loggedUser.username + '</span></a>')
            if(loggedUser.authorities[0].name == "admin"){
                $('#users').show();
                $("#users").css("display","block");
                $('#btnAdd').show();
            }

        }
    });
}

function subscribe(id,categoryId,token){
    console.log(categoryId);
    var data = {
        "categoryDTO":{
        		"id":categoryId
        	}
    }

    $.ajax({
        url:'http://localhost:8080/api/users/subscribe/' + id,
        type:'PUT',
        headers: { "Authorization": "Bearer " + token},
        data:JSON.stringify(data),
        contentType:'application/json',
        dataType:'json',
        crossDomain:true,
        success:function(response){
            logged(token);
            location.reload();

        }
    });
}

function unsubscribe(id,token){
    var data = {
        "categoryDTO":{
            "id": 1
        }
    }

    $.ajax({
        url:'http://localhost:8080/api/users/subscribe/' + id,
        type:'PUT',
        headers: { "Authorization": "Bearer " + token},
        data:JSON.stringify(data),
        contentType:'application/json',
        dataType:'json',
        crossDomain:true,
        success:function(response){
            logged(token);
            location.reload();
        }
    });
}

function addCategoryValidate(){
    var name = $('input[name=addCategory]').val();

    $('#addForm').validate({
        onfocusout:function(element){
            console.log(element);
            $(element).valid();
        },
        rules:{
            name:{
                required:true,
                minlength:2,
                maxlength:30
            }
        },
        messages:{
            name:{
                required:"Please enter category name",
                minlength:"Must enter at least 2 characters",
                maxlength:"Maximum number of characters is 30"
            }
        }
    });
}

function findEBooksByCategory(category_name,token){
    $.ajax({
        url:"http://localhost:8080/api/ebook/bycategory/" + category_name,
        type:'GET',
        contentType:'application/json',
        dataType:'json',
        crossDomain:true,
        success:function(response){
        console.log(response);

            for(var i=0; i<response.length; i++) {
                deleteEBookByCategory(eBook.filename,token);
            }
        }
    });
}

function deleteEBookByCategory(filename,token){
    var splitted = filename.split('files\\');
    console.log(splitted[1]);
    var splitted1 = splitted[1].split(".");
    console.log(splitted1[0]);
    var file = splitted1[0];

    $.ajax({
        url:'http://localhost:8080/api/indexer/removeFile/' + file,
        type:'GET',
        contentType:'application/json',
        crossDomain:true,
        processData: false,
        headers: { "Authorization": "Bearer " + token},
        success:function(response){
            console.log(response);
        },
        error:function(response){
            console.log(response);
        }
    });
}