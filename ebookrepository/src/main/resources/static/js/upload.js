$(document).ready(function(){
    sessionStorage.clear();
    var ebookId = window.location.search.slice(1).split('&')[0].split('=')[1];
    console.log(ebookId);
    var filename = '';

    $('#errorMessageFile').hide();
    $('#errorMessage').hide();
    var token = localStorage.getItem("token");
    if(token != null){
        $('#loginA').replaceWith('<a id="logout"><span class="glyphicon glyphicon-log-out"></span> LogOut</a>');
        logged(token);
    }

    if(ebookId != null){
        findEBookById(ebookId,token);
    }

    $('#logout').click(function(e){
        var confirmation = confirm('Are you sure you want to logout?');
        if(confirmation){
            localStorage.removeItem("token");
            localStorage.removeItem("loggedUser");
            window.location.replace('index.html');
        }
        e.preventDefault();
        return false;
    });

    $("#btnUpload").click(function (event) {
            //stop submit the form, we will post it manually.
        event.preventDefault();
        addEBookValidate();
        uploadData(token);
    });

    $("#btnSubmit").click(function(e){
        e.preventDefault();
        if(ebookId == null){
            addEBookValidate();
            index(token);
        }else{
            updateIndex(token);
            updateEBook(ebookId,token);
            localStorage.removeItem("eBook");
            window.location.replace("index.html");
        }

    });

    $(document).on('ready',function(){
        addEBookValidate();
    });

    if(ebookId == null){
        fillDropdown();
        fillLangDropdown(token);
    }

    $('#keepOnBtn').click(function(e){
        location.reload();
    });

    $('#goToBtn').click(function(e){
        window.location.replace("index.html");
    });

    window.onscroll = function() {stickyHeader()};

});



function uploadData(token){
    var uploadFile = document.getElementById("chooseFile");
    console.log("Upload file " + uploadFile.value.length);
    if(uploadFile.value.length == 0){
        $('#errorMessageFile').show();
    }else{
        var form = $('#fileUploadForm')[0];
        var data = new FormData(form);
        console.log(data);


        $.ajax({
            type:'POST',
            enctype: 'multipart/form-data',
            url: "api/indexer/getmodel",
            data:data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            headers: { "Authorization": "Bearer " + token},
            success:function(data){

                $('input[name=title]').val(data.title);
                $('input[name=keywords]').val(data.keywords);
                $('input[name=author]').val(data.author);

                filename = data.filename;
                console.log(filename);
            },
            error:function(e){
                console.log(e.responseText);
            }
        });

    }
}

function index(token){
    var uploadFile = document.getElementById("chooseFile");
    console.log("Upload file " + uploadFile.value.length);
    if(uploadFile.value.length == 0){
        $('#errorMessageFile').show();
    }else{

        var user = JSON.parse(localStorage.getItem("loggedUser"));
        var form = $('#fileUploadForm')[0];
        var data = new FormData(form);
        var language = $('#langDropdown option:selected').text();
        var category = $('#dropdown').val();
        data.append('language',language);
        data.append('category',category);
        var publicationYear = $('input[name=publication_year]').val();

        $("#btnSubmit").prop("disabled", true);

        $.ajax({
            type:'POST',
            enctype: 'multipart/form-data',
            url:"api/indexer/index/add",
            headers: { "Authorization": "Bearer " + token},
            data:data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success:function(data){
                var title = $('input[name=title]').val();
                var author = $('input[name=author]').val();
                var keywords = $('input[name=keywords]').val();
                var langValue = $('#langDropdown').val();
                var categoryDTO = {
                    "id": category
                };
                var languageDTO = {
                    "id": langValue
                };
                var userDTO = {
                    "id": user.id
                }
                var ebookData = {
                    'title': title,
                    'author':author,
                    'keywords':keywords,
                    'filename':filename,
                    'publication_year': publicationYear,
                    'categoryDTO':categoryDTO,
                    'languageDTO':languageDTO,
                    'userDTO': userDTO
                }
                $.ajax({
                    type:'POST',
                    url:'http://localhost:8080/api/ebook',
                    headers: { "Authorization": "Bearer " + token},
                    data:JSON.stringify(ebookData),
                    processData:false,
                    contentType:'application/json',
                    success:function(response){
                        console.log("Added EBook!")
                        document.getElementById('options').style.display='block';
                    },
                    error:function(response){
                        console.log(response);
                        if(response.status == 500){
                            $('#errorMessage').show();
                            $("#btnSubmit").prop("disabled", false);
                        }
                    }
                });
                $("#btnSubmit").prop("disabled", false);
            },
            error:function(response){
                console.log(response);
            }
        });
    }
}

function fillDropdown(){
    var dropdown = $('#dropdown');
    console.log(dropdown);
    var eBook = JSON.parse(localStorage.getItem("eBook"));
    $.ajax({
        type:'GET',
        url: "http://localhost:8080/api/category",
        contentType : "application/json",
        dataType:'json',
        crossDomain: true,
        success:function(response){
            for(var i = 1; i < response.length; i++) {
               category = response[i];
               dropdown.append('<option value='+category.id+'>'+category.name+'</option>');
            }

            if(eBook != null){
                console.log(eBook.categoryDTO.id);
                $("#dropdown").val(eBook.categoryDTO.id).change();
            }
        }
    });
}

function fillLangDropdown(token){
    var langDropdown = $('#langDropdown');
    console.log(langDropdown);
    var eBook = JSON.parse(localStorage.getItem("eBook"));
    $.ajax({
        type:'GET',
        url: "http://localhost:8080/api/languages",
        contentType:'application/json',
        dataType:'json',
        headers: { "Authorization": "Bearer " + token},
        crossDomain:true,
        success:function(response){
            for(var i = 0; i < response.length; i++) {
                language = response[i];
                langDropdown.append('<option value='+language.id+'>'+language.name+'</option>');
            }

            if(eBook != null){
                console.log(eBook.languageDTO.id);
                $("#langDropdown").val(eBook.languageDTO.id).change();
            }
        }
    });
}

function stickyHeader() {

    var header = document.getElementById("headerId");

    var sticky = header.offsetTop;

    if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
    } else {
        header.classList.remove("sticky");
    }
}



function logged(token){
    $.ajax({
        url: "http://localhost:8080/api/users/logged",
        contentType:'application/json',
        dataType:'json',
        crossDomain:true,
        processData: false,
        headers: { "Authorization": "Bearer " + token},
        success:function(response){
            var loggedUser = response;
            console.log(loggedUser);
            localStorage.setItem("loggedUser",JSON.stringify(loggedUser));
            $('#signUpA').replaceWith('<a href="user.html?user=' + loggedUser.id + '" id="user"><span>' + loggedUser.username + '</span></a>')
            if(loggedUser.authorities[0].name == "admin"){
                $('#users').show();
                $("#users").css("display","block");
            }
        }
    })
}

function findEBookById(id,token){
    console.log("FindByID");
    $.ajax({
        url: 'http://localhost:8080/api/ebook/' + id,
        type:'GET',
        contentType:'application/json',
        dataType:'json',
        crossDomain:true,
        success:function(response){
            eBook = response;
            console.log(eBook);
            filename = eBook.filename;
            localStorage.setItem("eBook",JSON.stringify(eBook));

            $('input[name=title]').val(eBook.title);
            $('input[name=author]').val(eBook.author);
            $('input[name=keywords]').val(eBook.keywords);
            $('input[name=publication_year]').val(eBook.publication_year);

            fillDropdown();
            fillLangDropdown(token);

            $('#uploadDiv').hide();
        }
    });
}

function updateIndex(token){
    var splitted = filename.split('files\\');
    var splitted1 = splitted[1].split(".");
    var file = splitted1[0];
    var lang = $('#langDropdown option:selected').text();
    console.log(lang);
    var data = {
        'title': $('input[name=title]').val(),
        'author': $('input[name=author]').val(),
        'filename': filename,
        'keywords': $('input[name=keywords]').val(),
        'language': lang,
        'category': $('#dropdown').val()
    }

    console.log(JSON.stringify(data));

    $.ajax({
        url:'http://localhost:8080/api/indexer/updateIndex/' + file,
        type:'PUT',
        crossDomain:true,
        contentType: 'application/json',
        data:JSON.stringify(data),
        processData: false,
        headers: { "Authorization": "Bearer " + token},
        success:function(response){
            console.log(response);
        }
    });
}

function updateEBook(id,token){
    var user = JSON.parse(localStorage.getItem("loggedUser"));

    var data = {
        'title': $('input[name=title]').val(),
        'author': $('input[name=author]').val(),
        'keywords': $('input[name=keywords]').val(),
        'publication_year': $('input[name=publication_year]').val(),
        'filename': filename,
        'languageDTO': {
            'id': $('#langDropdown').val(),
        },
        'categoryDTO':{
            'id': $('#dropdown').val()
        },
        'userDTO': {
            'id': user.id
        }
    }

    $.ajax({
        url:'http://localhost:8080/api/ebook/' + id,
        type:'PUT',
        crossDomain:true,
        contentType: 'application/json',
        data:JSON.stringify(data),
        processData: false,
        headers: { "Authorization": "Bearer " + token},
        success:function(response){
            console.log(response);
        }
    });
}

function addEBookValidate(){
    var title = $('input[name=title]').val();
    var author = $('input[name=author]').val();
    var keywords = $('input[name=keywords]').val();
    var year = $('input[name=publication_year]').val();

    $('#fileUploadForm').validate({
        onfocusout:function(element){
            console.log(element);
            $(element).valid();
        },
        rules:{
            title:{
                required:true,
                minlength:2,
                maxlength:30
            },
            author:{
                required:true,
                minlength:2,
                maxlength:30
            },
            keywords:{
                required:true,
                minlength:2,
                maxlength:30
            },
            year:{
                required:true,
                minlength:4,
                number:true
            }
        },
        messages:{
            title:{
                required:"Please enter title",
                minlength:"Must enter at least 2 characters",
                maxlength:"Maximum number of characters is 30"
            },
            author:{
                required:"Please enter author",
                minlength:"Must enter at least 2 characters",
                maxlength:"Maximum number of characters is 30"
            },
            keywords:{
                required:"Please enter keywords",
                minlength:"Must enter at least 2 characters",
                maxlength:"Maximum number of characters is 30"
            },
            year:{
                required:"Please enter year",
                minlength:"Maximum number of characters is 4",
                number:"Year must be a number"
            }
        }
    });
}