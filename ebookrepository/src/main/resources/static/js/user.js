$(document).ready(function(){
    sessionStorage.clear();
    var userId = window.location.search.slice(1).split('&')[0].split('=')[1];

    $('#users').hide();
    $('.overlay').hide();

    $('#loginA').click(function(e){
        document.getElementById('login').style.display='block';
    });

    $('#signUpA').click(function(e){
        document.getElementById('register').style.display='block';
    });

    var token = localStorage.getItem("token");
    if(token != null){
        $('#loginA').replaceWith('<a id="logout"><span class="glyphicon glyphicon-log-out"></span> LogOut</a>')
    }

    if(token == null){
        $('#btnChangePass').hide();
        $('#btnEditProfile').hide();
        $('.userInfo').hide();
        $('.overlay').show();
    }

    logged(token);
    findUserById(userId,token);

    $('#logout').click(function(e){
        var confirmation = confirm('Are you sure you want to logout?');
        if(confirmation){
            localStorage.removeItem("token");
            window.location.replace("index.html");
        }
        e.preventDefault();
        return false;
    });

    var modalEdit = document.getElementById('editProfile');
    window.onclick = function(event) {
        if (event.target == modalEdit) {
            modalEdit.style.display = "none";
        }
    }

    var modalChangePass = document.getElementById('changePass');
    window.onclick = function(event) {
        if (event.target == modalChangePass) {
            modalChangePass.style.display = "none";
        }
    }
    fillDropdown();

    $('#btnEditProfile').click(function(event){
        event.preventDefault();

        var modalAdd = document.getElementById('editProfile');
        modalAdd.style.display = "block";
    });

    $('#editBtn').click(function(event){
        event.preventDefault();
        editUserValidate();
        updateUser(userId,token);
    });

    $('#changePassBtn').click(function(event){
        event.preventDefault();
        changePassword(token);
    });

    $(document).on('ready',function(){
        editUserValidate();
    });


});

function findUserById(id,token){
    var container = $('.userInfo');

    $.ajax({
        url:'http://localhost:8080/api/users/' + id,
        type:'GET',
        contentType:'application/json',
        dataType:'json',
        headers: { "Authorization": "Bearer " + token},
        crossDomain:true,
        success:function(response){
            user = response;
            container.append(
                '<label id="fName">First Name: ' + user.first_name + '</label>' +
                '<label id="lName">Last Name: ' + user.last_name + '</label>' +
                '<label id="username">Username: ' + user.username + '</label>' +
                '<label id="userType">User Type: ' + user.authority + '</label>' +
                '<label id="category">Category: ' + user.categoryDTO.name + '</label>'
            );

            var first_name = $('input[name=fname]').val(user.first_name);
            var last_name = $('input[name=lname]').val(user.last_name);
            var username = $('input[name=username]').val(user.username);
            var category = $("#dropdown").val(user.categoryDTO.id).change();
        }
    });
}

function logged(token){
    $.ajax({
        url: "http://localhost:8080/api/users/logged",
        contentType:'application/json',
        dataType:'json',
        crossDomain:true,
        processData: false,
        headers: { "Authorization": "Bearer " + token},
        success:function(response){
            var loggedUser = response;
            console.log(loggedUser);
            //localStorage.setItem("loggedUser",JSON.stringify(loggedUser));
            $('#signUpA').replaceWith('<a href="user.html?user=' + loggedUser.username + '" id="user"><span>' + loggedUser.username + '</span></a>')
            if(loggedUser.authorities[0].name == "admin"){
                $('#users').show();
                $("#users").css("display","block");
            }
        }
    })
}

function changePassword(token){
    var oldPass = $('#oldPass').val().trim();
    var newPass = $('#newPass').val().trim();
    var confirmPass = $('#cPass').val().trim();

    if(newPass != confirmPass){
        alert("Password do not match!");
    }else{

        var data = {
            'oldPassword': oldPass,
            'newPassword': newPass
        }

        $.ajax({
            url: 'http://localhost:8080/api/auth/changePassword',
            type:'POST',
            contentType:'application/json',
            headers: { "Authorization": "Bearer " + token},
            dataType:'json',
            data:JSON.stringify(data),
            crossDomain:true,
            processData: false,
            success:function(response){
                console.log(response);
            }
        });

        alert('Password changed');
        var modalChangePass = document.getElementById('changePass');
        modalChangePass.style.display = "none";

    }
}

function updateUser(id,token){
    var first_name = $('input[name=fname]').val();
    var last_name = $('input[name=lname]').val();
    var username = $('input[name=username]').val();
    var category = $('#dropdown').val();

    var data = {
        'first_name':first_name,
        'last_name':last_name,
        'username':username,
        'categoryDTO': {
            'id': category
        }
    }

    $.ajax({
        type:'PUT',
        headers: { "Authorization": "Bearer " + token},
        url:'http://localhost:8080/api/users/' + id,
        data:JSON.stringify(data),
        contentType:'application/json',
        dataType:'json',
        crossDomain:true,
        success:function(response){
            console.log("UPDATE")
            console.log(response);
            user = response;
            document.getElementById('editProfile').style.display='none';
            logged(token);

            $('#fName').text('First Name: ' + user.first_name);
            $('#lName').text('Last Name: ' + user.last_name);
            $('#username').text('Username: ' + user.username);
            $('#userType').text('User Type: ' + user.authority);
            $('#category').text('Category: ' + user.categoryDTO.name);

        },
        error: function(response) {
            if(response.status == 400){
                console.log(response);
                $('#errorMessage').show();
            }
        }
    });
}

function editUserValidate(){
    var first_name = $('input[name=fname]').val();
    var last_name = $('input[name=lname]').val();
    var username = $('input[name=username]').val();

    console.log("Validacija");
    $('#editForm').validate({
        onfocusout:function(element){
            console.log(element);
            $(element).valid();
        },
        rules:{
            first_name:{
                required:true,
                minlength:2,
                maxlength:30

            },
            last_name:{
                required:true,
                minlength:2,
                maxlength:30

            },
            username:{
                required:true,
                minlength:2,
                maxlength:10
            }
        },
        messages:{
            first_name:{
                required:"Please enter your name",
                minlength:"Must enter at least 2 characters",
                maxlength:"Maximum number of characters is 30"

            },
            last_name:{
                required:"Please enter your last name",
                minlength:"Must enter at least 2 characters",
                maxlength:"Maximum number of characters is 30"

            },
            username:{
                required:"Please enter your username",
                minlength:"Must enter at least 2 characters",
                maxlength:"Maximum number of characters is 30"
            }
        }
    })
}