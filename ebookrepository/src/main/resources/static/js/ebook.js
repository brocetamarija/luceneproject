$(document).ready(function(){
    var token = localStorage.getItem("token");
    if(token != null){
        $('#loginA').replaceWith('<a id="logout"><span class="glyphicon glyphicon-log-out"></span> LogOut</a>');
        logged(token);
    }
    if(token == null){
        document.getElementById('options').style.display='block';
    }
    $('#btnDownload').hide();
    $('#btnUpdate').hide();
    $('#btnDelete').hide();
    $('#users').hide();

    var param_name = window.location.search.slice(1).split('=')[0];
    if(param_name == 'id'){
        var eBookId = window.location.search.slice(1).split('&')[0].split('=')[1];
        findEBookById(eBookId);
    }else if(param_name == 'filename'){
        var filename = window.location.search.slice(1).split('&')[0].split('=')[1];
        findEBookByFilename(filename);
    }

    $('#loginA').click(function(e){
        document.getElementById('login').style.display='block';
    });

    $('#signUpA').click(function(e){
        document.getElementById('register').style.display='block';
    });

    $('#logout').click(function(e){
        var confirmation = confirm('Are you sure you want to logout?');
        if(confirmation){
            localStorage.removeItem("token");
            localStorage.removeItem("loggedUser");
            $('#logout').replaceWith('<a class="active" id="loginA">Login</a>');
            $('#user').replaceWith('<a class="active" id="signUpA">Sign Up</a>');

            $('#users').hide();
            location.reload();
        }
        e.preventDefault();
        return false;
    });

    var loginBtn = $('#loginBtn');
    loginBtn.on('click',function(e){
        login();
        e.preventDefault();
        return false;
    });

    $('#registerBtn').click(function(event){
        event.preventDefault();
        register();
    });


    var modal = document.getElementById('login');
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

    var modalR = document.getElementById('register');
    window.onclick = function(event) {
        if (event.target == modalR) {
            modalR.style.display = "none";
        }
    }

    $('#btnDownload').click(function(e){
        download(token);
    })

    $('#btnDelete').click(function(e){
        if(param_name == 'id'){
            var eBookId = window.location.search.slice(1).split('&')[0].split('=')[1];
            deleteEBook(eBookId,token);
        }else if(param_name == 'filename'){
            var filename = window.location.search.slice(1).split('&')[0].split('=')[1];
            deleteEBookByFilename(filename,token);
        }
        console.log(filename);
        deleteFromFile(token);
    });

    $('#btnUpdate').click(function(e){
        var eBook = JSON.parse(sessionStorage.getItem("eBook"));
        window.location.replace("upload.html?id=" + eBook.id);
    });

    $('#downloadLoginBtn').click(function(e){
        document.getElementById('options').style.display='none';
        document.getElementById('login').style.display='block';
    });

    $('#downloadRegisterBtn').click(function(e){
        document.getElementById('options').style.display='none';
        document.getElementById('register').style.display='block';
    });

    $('#downloadCatBtn').click(function(e){
        window.location.replace('category.html');
    });

    fillDropdown();

});

function findEBookById(id){
    var container = $('.eBookInfo');
    var loggedUser = JSON.parse(localStorage.getItem("loggedUser"));
    $.ajax({
        url: 'http://localhost:8080/api/ebook/' + id,
        type:'GET',
        contentType:'application/json',
        dataType:'json',
        crossDomain:true,
        success:function(response){
            eBook = response;
            console.log(eBook);
            if(loggedUser != null){
                if(loggedUser.category.name == 'None' || loggedUser.category.name == eBook.categoryDTO.name){
                    $('#btnDownload').show();
                }else{
                    document.getElementById('goToCat').style.display='block';
                }
            }
            sessionStorage.setItem("eBook",JSON.stringify(eBook));
            container.append(
                '<label id="titleLbl">Title: ' + eBook.title + ' </label>' +
                '<label id="authorLbl">Author: ' + eBook.author + ' </label>' +
                '<label id="yearLbl">Publication year: ' + eBook.publication_year + ' </label>' +
                '<label id="categoryLbl">Category: ' + eBook.categoryDTO.name +  ' </label>' +
                '<label id="languageLbl">Language: ' + eBook.languageDTO.name +  ' </label>' +
                '<label id="keywords">Keywords: ' + eBook.keywords + ' </label>'

            );
        }
    });


}

function findEBookByFilename(filename){
    var container = $('.eBookInfo');
    var loggedUser = JSON.parse(localStorage.getItem("loggedUser"));

    $.ajax({
        url: 'http://localhost:8080/api/ebook/findByFilename/' + filename,
        type:'GET',
        contentType:'application/json',
        dataType:'json',
        crossDomain:true,
        success:function(response){
            eBook = response;
            console.log(eBook);
            if(loggedUser != null){
                if(loggedUser.category.name == 'None' || loggedUser.category.name == eBook.categoryDTO.name){
                    $('#btnDownload').show();
                }
            }else{
                document.getElementById('goToCat').style.display='block';
            }
            sessionStorage.setItem("eBook",JSON.stringify(eBook));
            container.append(
                '<label id="titleLbl">Title: ' + eBook.title + ' </label>' +
                '<label id="authorLbl">Author: ' + eBook.author + ' </label>' +
                '<label id="yearLbl">Publication year: ' + eBook.publication_year + ' </label>' +
                '<label id="categoryLbl">Category: ' + eBook.categoryDTO.name +  ' </label>' +
                '<label id="languageLbl">Language: ' + eBook.languageDTO.name +  ' </label>' +
                '<label id="keywords">Keywords: ' + eBook.keywords + ' </label>'
            );
        }
    });
}

function logged(token){

    $.ajax({
        url: "http://localhost:8080/api/users/logged",
        contentType:'application/json',
        dataType:'json',
        crossDomain:true,
        processData: false,
        headers: { "Authorization": "Bearer " + token},
        success:function(response){

            var loggedUser = response;
            console.log(loggedUser);
            localStorage.setItem("loggedUser",JSON.stringify(loggedUser));
            $('#signUpA').replaceWith('<a href="user.html?user=' + loggedUser.id + '" id="user"><span>' + loggedUser.username + '</span></a>')
            if(loggedUser.authorities[0].name == "admin"){
                $('#users').show();
                $("#users").css("display","block");
                $('#btnUpdate').show();
                $('#btnDelete').show();
            }
        }
    });
}

function download(token){
    var eBook = JSON.parse(sessionStorage.getItem("eBook"));
    console.log(eBook);
    console.log(eBook.filename);
    var eBookFilename = eBook.filename;
    var splitted = eBookFilename.split('files\\');
    console.log(splitted[1]);
    var splitted1 = splitted[1].split(".");
    console.log(splitted1[0]);
    var file = splitted1[0];

    var xhr = new XMLHttpRequest();
    xhr.open('GET', "/api/indexer/download/" + file, true);
    xhr.setRequestHeader("Authorization", "Bearer " + token);
    xhr.responseType = 'blob';

    xhr.onload = function(e){
        if(this.status == 200){
            var blob = this.response;
            console.log(blob);
            var a = document.createElement('a');
            var url = window.URL.createObjectURL(blob);
            a.href = url;
            a.download = xhr.getResponseHeader('filename');
            a.click();
            window.URL.revokeObjectURL(url);
        }
    };

    xhr.send();
}



function deleteFromFile(token){
    var eBook = JSON.parse(sessionStorage.getItem("eBook"));
    console.log(eBook);
    console.log(eBook.filename);
    var eBookFilename = eBook.filename;
    var splitted = eBookFilename.split('files\\');
    console.log(splitted[1]);
    var splitted1 = splitted[1].split(".");
    console.log(splitted1[0]);
    var file = splitted1[0];

    $.ajax({
        url:'http://localhost:8080/api/indexer/removeFile/' + file,
        type:'GET',
        contentType:'application/json',
        crossDomain:true,
        processData: false,
        headers: { "Authorization": "Bearer " + token},
        success:function(response){
            console.log(response);
        }
    });
}

function deleteEBook(id,token){


    $.ajax({
        url:'http://localhost:8080/api/ebook/' + id,
        contentType:'application/json',
        dataType:'json',
        type:'DELETE',
        crossDomain:true,
        processData: false,
        headers: { "Authorization": "Bearer " + token},
        success:function(response){

        }
    });
}

function deleteEBookByFilename(file,token){

    $.ajax({
        url:'http://localhost:8080/api/ebook/deleteByFilename/' + file,
        contentType:'application/json',
        dataType:'json',
        type:'DELETE',
        crossDomain:true,
        processData: false,
        headers: { "Authorization": "Bearer " + token},
        success:function(response){

        }
    });
}